using System;
using OnlineServices.Abstractions;
using OnlineServices.Common.Droid;
using OnlineServices.Data;
using OnlineServices.Models;
using OnlineServices.Util;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Autofac;
using Autofac.Extras.CommonServiceLocator;
using FFImageLoading.Forms.Droid;
using HockeyApp.Android;
using Microsoft.Practices.ServiceLocation;
using Xamarin;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using OnlineServices.Models.DTO;
using OnlineServices.Models.Filters;
using OnlineServices.ModelContracts;
using OnlineServices.Data.Remote;

namespace Acquaint.XForms.Droid
{
	[Activity (Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : FormsAppCompatActivity
	// inhertiting from FormsAppCompatActivity is imperative to taking advantage of Android AppCompat libraries
	{
		// an IoC Container
		IContainer _IoCContainer;

		protected override void OnCreate (Bundle bundle)
		{
			RegisterDependencies();

			Settings.OnDataPartitionPhraseChanged += (sender, e) => {
				UpdateDataSourceIfNecessary();
			};

			// Azure Mobile Services initilizatio
			Microsoft.WindowsAzure.MobileServices.CurrentPlatform.Init();

			CachedImageRenderer.Init();

			// this line is essential to wiring up the toolbar styles defined in ~/Resources/layout/toolbar.axml
			FormsAppCompatActivity.ToolbarResource = Resource.Layout.toolbar;

			base.OnCreate (bundle);

			// register HockeyApp as the crash reporter
			//CrashManager.Register(this, Settings.HockeyAppId);

			Forms.Init (this, bundle);

			FormsMaps.Init (this, bundle);

			LoadApplication (new App ());
		}

        /// <summary>
        /// Registers dependencies with an IoC container.
        /// </summary>
        /// <remarks>
        /// Since some of our libraries are shared between the Forms and Native versions 
        /// of this app, we're using an IoC/DI framework to provide access across implementations.
        /// </remarks>
        void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance(new EnvironmentService()).As<IEnvironmentService>();

            builder.RegisterInstance(new HttpClientHandlerFactory()).As<IHttpClientHandlerFactory>();

            // Set the data source dependent on whether or not the data parition phrase is "UseLocalDataSource".
            // The local data source is mainly for use in TestCloud test runs, but the app can be used in local-only data mode if desired.
            if (Settings.IsUsingLocalDataSource)
            {
                builder.RegisterInstance(LazyLocalCategoryDataSource.Value)
                    .As<IDataSource<Category, CategoryFilter>>();
                builder.RegisterInstance(LazyLocalSubCategoryDataSource.Value)
                    .As<IDataSource<SubCategory, SubCategoryFilter>>();
                builder.RegisterInstance(LazyLocalServiceProviderDataSource.Value)
                    .As<IDataSource<ServiceProviderDTO, ServiceProviderFilter>>();
            }
            else
            {
                builder.RegisterInstance(LazyCategoryDataSource.Value)
                    .As<IDataSource<Category, CategoryFilter>>();
                builder.RegisterInstance(LazySubCategoryDataSource.Value)
                    .As<IDataSource<SubCategory, SubCategoryFilter>>();
                builder.RegisterInstance(LazyServiceProviderDataSource.Value)
                    .As<IDataSource<ServiceProviderDTO, ServiceProviderFilter>>();
            }

            _IoCContainer = builder.Build();

            var csl = new AutofacServiceLocator(_IoCContainer);
            ServiceLocator.SetLocatorProvider(() => csl);
        }

        /// <summary>
		/// Updates the data source if necessary.
		/// </summary>
		void UpdateDataSourceIfNecessary()
        {
            var dataSource = ServiceLocator.Current.GetInstance<IDataSource<SubCategory, SubCategoryFilter>>();

			// Set the data source dependent on whether or not the data parition phrase is "UseLocalDataSource".
			// The local data source is mainly for use in TestCloud test runs, but the app can be used in local-only data mode if desired.

			// if the settings dictate that a local data source should be used, then register the local data provider and update the IoC container
			if (Settings.IsUsingLocalDataSource)
            {
                var builder = new ContainerBuilder();
                builder.RegisterInstance(LazyLocalCategoryDataSource.Value).As<IDataSource<Category, CategoryFilter>>();
                builder.RegisterInstance(LazyLocalSubCategoryDataSource.Value).As<IDataSource<SubCategory, SubCategoryFilter>>();
                builder.RegisterInstance(LazyLocalServiceProviderDataSource.Value).As<IDataSource<ServiceProviderDTO, ServiceProviderFilter>>();
                builder.Update(_IoCContainer);
                return;
            }

            // if the settings dictate that a local data souce should not be used, then register the remote data source and update the IoC container
            if (!Settings.IsUsingLocalDataSource)
            {
                var builder = new ContainerBuilder();
                builder.RegisterInstance(LazyCategoryDataSource.Value).As<IDataSource<Category, CategoryFilter>>();
                builder.RegisterInstance(LazySubCategoryDataSource.Value).As<IDataSource<SubCategory, SubCategoryFilter>>();
                builder.RegisterInstance(LazyServiceProviderDataSource.Value).As<IDataSource<ServiceProviderDTO, ServiceProviderFilter>>();
                builder.Update(_IoCContainer);
            }
        }

        // we need lazy loaded instances of these two types hanging around because if the registration on IoC container changes at runtime, we want the same instances
        //REMOTE //Sub Category is reading data locally
	    private static readonly Lazy<LocalCategoryDataSource> LazyCategoryDataSource = new Lazy<LocalCategoryDataSource>(() => new LocalCategoryDataSource());
	    private static readonly Lazy<SubCategoryDataSource> LazySubCategoryDataSource = new Lazy<SubCategoryDataSource>(() => new SubCategoryDataSource());
	    private static readonly Lazy<ServiceProviderDataSource> LazyServiceProviderDataSource = new Lazy<ServiceProviderDataSource>(() => new ServiceProviderDataSource());
        //LOCAL
	    private static readonly Lazy<LocalCategoryDataSource> LazyLocalCategoryDataSource = new Lazy<LocalCategoryDataSource>(() => new LocalCategoryDataSource());
	    private static readonly Lazy<LocalSubCategoryDataSource> LazyLocalSubCategoryDataSource = new Lazy<LocalSubCategoryDataSource>(() => new LocalSubCategoryDataSource());
	    private static readonly Lazy<LocalServiceProviderDataSource> LazyLocalServiceProviderDataSource = new Lazy<LocalServiceProviderDataSource>(() => new LocalServiceProviderDataSource());
    }    
}
