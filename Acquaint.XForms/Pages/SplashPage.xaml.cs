﻿using Xamarin.Forms;
using System.Threading.Tasks;
using OnlineServices.Util;
using Acquaint.XForms.Pages;
using Acquaint.XForms.ViewModels;

namespace Acquaint.XForms
{
    /// <summary>
    /// Splash Page that is used on Android only. iOS splash characteristics are NOT defined here, ub tn the iOS prject settings.
    /// </summary>
    public partial class SplashPage : ContentPage
    {
        bool _ShouldDelayForSplash = true;

        public SplashPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            if (_ShouldDelayForSplash)
                await Task.Delay(3000);

            if (!Settings.IsLocationSelected)
            {
                var navPage = new NavigationPage(new ChangeLocationPage()
                {
                    Title = "Choose Location",
                    BindingContext = new SettingsViewModel()
                });

                await Navigation.PushModalAsync(navPage);

                _ShouldDelayForSplash = false;
            }
            else
            {

                var navPage = new NavigationPage(new CategoryListPage()
                {
                    Title = "Categories",
                    BindingContext = new CategoryListViewModel()
                });

                Application.Current.MainPage = navPage;
            }

        }
    }
}

