﻿using Acquaint.XForms.ViewModels;
using OnlineServices.Models;
using OnlineServices.Util;
using System;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Acquaint.XForms.Pages
{
    public partial class CategoryListPage : ContentPage
    {
        protected CategoryListViewModel ViewModel => BindingContext as CategoryListViewModel;
        public bool GridInitialized;

        public CategoryListPage()
        {
            InitializeComponent();

            if (Device.RuntimePlatform != Device.WinPhone)
            {
                ToolbarItems.Remove(refreshToolbarItem);
            }
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (!Settings.IsLocationSelected && Device.RuntimePlatform == Device.iOS)
            {
                var navPage = new NavigationPage(new ChangeLocationPage()
                {
                    Title = "Choose Location",
                    BindingContext = new SettingsViewModel()
                });

                await Navigation.PushModalAsync(navPage);
            }
            else
            {
                await ViewModel.ExecuteLoadCategoriesCommand();

                if (!GridInitialized)
                {
                    InitializeGrid();

                    if (Settings.UserLocation.Equals("UseCurrentLocation"))
                    {
                        var userCoor = await LocationUtility.GetCurrentPosition(1000);
                        Settings.UserSelectedLocationCoordinates =
                            userCoor != null ? $"{userCoor.Latitude};{userCoor.Longitude}" : string.Empty;
                    }
                    GridInitialized = true;
                }
            }
        }

        void InitializeGrid()
        {
            var categories = ViewModel.Categories;
            CategoriesGrid.RowDefinitions = new RowDefinitionCollection();
            CategoriesGrid.ColumnDefinitions = new ColumnDefinitionCollection();
            CategoriesGrid.Children.Clear();

            CategoriesGrid.ColumnSpacing = 1;
            CategoriesGrid.RowSpacing = 1;
            CategoriesGrid.ColumnDefinitions.Add(new ColumnDefinition());
            CategoriesGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            CategoriesGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            CategoriesGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            CategoriesGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            CategoriesGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });

            var row = 0;
            foreach (var category in categories)
            {
                var itemTaped = new TapGestureRecognizer
                {
                    Command = new Command(() => ItemTapped(category))
                };

                if (Device.RuntimePlatform == Device.Android)
                {
                    var image = new Image
                    {
                        Source = category.IconUrl,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        BackgroundColor = Color.Gray,
                        Aspect = Aspect.AspectFill
                    };

                    var view = new StackLayout()
                    {
                        Padding = 0,
                        Children =
                        {
                            image
                        }
                    };

                    var frame = new Frame
                    {
                        Padding = 0,
                        Margin = new Thickness(10, 5, 10, 5),
                        CornerRadius = 5,
                        BackgroundColor = Color.Gray,
                        Content = view,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HeightRequest = 150,
                        HasShadow = true,
                        OutlineColor = Color.Gray
                    };
                    frame.GestureRecognizers.Add(itemTaped);
                    CategoriesGrid.Children.Add(frame, 0, row);
                }
                else if (Device.RuntimePlatform == Device.iOS)
                {
                    var image = new Image
                    {
                        Source = category.IconUrl,
                        Margin = new Thickness(10, 5, 10, 5),
                        HorizontalOptions = LayoutOptions.Fill,
                        VerticalOptions = LayoutOptions.Fill,
                        BackgroundColor = Color.Gray,
                        Aspect = Aspect.AspectFill
                    };
                    image.GestureRecognizers.Add(itemTaped);
                    CategoriesGrid.Children.Add(image, 0, row);
                }

                row++;
            }

            ScrollView.IsVisible = true;
        }

        void ItemTapped(Category item)
        {
            if (item.Name.Equals("job"))
            {
                Navigation.PushAsync(new JobsListPage()
                {
                    Title = $"{item.Title} - {item.ArabicTitle}",
                    BindingContext = new ServiceProviderListViewModel(new SubCategory() { Id = item.Id, Name = item.Name })
                });
            }
            else if (item.Name.Equals("foodsanddrinks"))
            {
                Navigation.PushAsync(new FoodsAndDrinksListPage()
                {
                    Title = $"{item.Title} - {item.ArabicTitle}",
                    BindingContext = new SubCategoryListViewModel(item)
                });
            }
            else if (item.Name.Equals("apartment") || item.Name.Equals("shopping"))
            {
                Navigation.PushAsync(new ServiceProviderListPage()
                {
                    Title = $"{item.Title} - {item.ArabicTitle}",
                    BindingContext = new ServiceProviderListViewModel(new SubCategory() { Id = item.Id, Name = item.Name })
                });
            }
            else
            {
                Navigation.PushAsync(new SubCategoryListPage()
                {
                    Title = $"{item.Title} - {item.ArabicTitle}",
                    BindingContext = new SubCategoryListViewModel(item)
                });
            }
        }
    }
}