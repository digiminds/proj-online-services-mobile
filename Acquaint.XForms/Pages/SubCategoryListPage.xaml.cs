﻿using Acquaint.XForms.Converters;
using Acquaint.XForms.ViewModels;
using OnlineServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlineServices.Util;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;


namespace Acquaint.XForms.Pages
{
    public partial class SubCategoryListPage : ContentPage
    {
        protected SubCategoryListViewModel ViewModel => BindingContext as SubCategoryListViewModel;
        public bool GridInitialized = false;

        public SubCategoryListPage()
        {
            InitializeComponent();

            if (Device.RuntimePlatform != Device.WinPhone)
            {
                ToolbarItems.Remove(refreshToolbarItem);
            }
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await ViewModel.ExecuteLoadSubCategoriesCommand();

            searchBar.Placeholder = "Search...";
            searchBar.HorizontalOptions = LayoutOptions.FillAndExpand;
            searchBar.SearchCommand = new Command(async () =>
            {
                await ViewModel.ExecuteSearchCommand(searchBar.Text);
                InitializeGrid();
            });
            searchBar.TextChanged += async (object sender, TextChangedEventArgs e) =>
            {
                if (string.IsNullOrEmpty(e.NewTextValue))
                {
                    await ViewModel.ExecuteRefreshSubCategoriesCommand();
                    InitializeGrid();
                }
            };
            if (!GridInitialized)
            {
                InitializeGrid();
                GridInitialized = true;
            }
        }

        public void RefreshSubCategories(object sender, EventArgs e)
        {
            Refresh();
        }

        private async Task Refresh()
        {
            await ViewModel.ExecuteRefreshSubCategoriesCommand();
            InitializeGrid();
        }

        void InitializeGrid()
        {
            var subcategories = ViewModel.SubCategories;
            subCategoriesGrid.RowDefinitions = new RowDefinitionCollection();
            subCategoriesGrid.ColumnDefinitions = new ColumnDefinitionCollection();
            subCategoriesGrid.Children.Clear();

            subCategoriesGrid.ColumnSpacing = 1;
            subCategoriesGrid.RowSpacing = 1;
            subCategoriesGrid.RowDefinitions.Add(new RowDefinition
            {
                Height = new GridLength(1, GridUnitType.Auto)
            });
            subCategoriesGrid.ColumnDefinitions.Add(new ColumnDefinition());
            subCategoriesGrid.ColumnDefinitions.Add(new ColumnDefinition());
            subCategoriesGrid.ColumnDefinitions.Add(new ColumnDefinition());

            int row = 0, col = 0;
            foreach (var subCategory in subcategories)
            {
                var itemTaped = new TapGestureRecognizer
                {
                    Command = new Command(() => ItemTapped(subCategory))
                };

                var image = new Image
                {
                    Source = subCategory.IconUrl,
                    VerticalOptions = LayoutOptions.Start,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    BackgroundColor = Color.Transparent,
                    HeightRequest = 100
                };

                var name = new Label
                {
                    Text = subCategory.Title,
                    TextColor = Color.Black,
                    BackgroundColor = Color.Transparent,
                    FontSize = 10,
                    HorizontalTextAlignment = TextAlignment.Center,
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.CenterAndExpand
                };

                var arabicName = new Label
                {
                    Text = subCategory.ArabicTitle,
                    TextColor = Color.Black,
                    BackgroundColor = Color.Transparent,
                    FontSize = 10,
                    HorizontalTextAlignment = TextAlignment.Center,
                    VerticalOptions = LayoutOptions.End,
                    HorizontalOptions = LayoutOptions.CenterAndExpand
                };

                var view = new StackLayout
                {
                    Children =
                    {
                        image, name, arabicName
                    }
                };

                var frame = new Frame
                {
                    Padding = 0,
                    Margin = 3,
                    CornerRadius = 5,
                    BackgroundColor = Color.WhiteSmoke,
                    Content = view,
                    VerticalOptions = LayoutOptions.Start,
                    HeightRequest = 160,
                    HasShadow = true,
                    OutlineColor = Color.Black
                };
                frame.GestureRecognizers.Add(itemTaped);

                if (Device.RuntimePlatform == Device.iOS)
                {
                    frame.HasShadow = false;
                    frame.OutlineColor = Color.WhiteSmoke;
                }

                subCategoriesGrid.Children.Add(frame, col, row);
                col++;

                if (col % 3 == 0)
                {
                    col = 0;
                    row++;
                }
            }
        }

        void ItemTapped(SubCategory item)
        {
            Navigation.PushAsync(new ServiceProviderListPage()
            {
                Title = item.Title,
                BindingContext = new ServiceProviderListViewModel(item)
            });
        }

        void ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Navigation.PushAsync(new ServiceProviderListPage()
            {
                Title = ((SubCategory)e.Item).Title,
                BindingContext = new ServiceProviderListViewModel((SubCategory)e.Item)
            });
        }
    }
}
