﻿using DurianCode.PlacesSearchBar;
using OnlineServices.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Acquaint.XForms.Pages
{
    public partial class ChangeLocationPage : ContentPage
    {
        protected SettingsViewModel ViewModel => BindingContext as SettingsViewModel;

        protected override void OnAppearing()
        {
            base.OnAppearing();
            orLabel.Text = "Or";
            orLabel.FontSize = 11;
            orLabel.TextColor = Color.Black;
            orLabel.HorizontalOptions = LayoutOptions.CenterAndExpand;

            searchBar.HorizontalOptions = LayoutOptions.FillAndExpand;
        }

        public ChangeLocationPage()
        {
            InitializeComponent();
            searchBar.ApiKey = Settings.GooglePlacesApiKey;
            searchBar.Type = PlaceType.All;
            searchBar.Components = new Components("country:lb");
            searchBar.PlacesRetrieved += PlacesRetrieved;
            searchBar.TextChanged += PlacesTextChanged;
            searchBar.MinimumSearchText = 3;
            resultsList.ItemSelected += PlacesItemSelected;
        }

        void PlacesRetrieved(object sender, AutoCompleteResult result)
        {
            resultsList.ItemsSource = result.AutoCompletePlaces;
            spinner.IsRunning = false;
            spinner.IsVisible = false;

            if (result.AutoCompletePlaces != null && result.AutoCompletePlaces.Count > 0)
                resultsList.IsVisible = true;
        }

        void PlacesTextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.NewTextValue))
            {
                resultsList.IsVisible = false;
                spinner.IsVisible = true;
                spinner.IsRunning = true;
            }
            else
            {
                resultsList.IsVisible = true;
                spinner.IsRunning = false;
                spinner.IsVisible = false;
            }
        }

        async void PlacesItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;

            var prediction = (AutoCompletePrediction)e.SelectedItem;
            resultsList.SelectedItem = null;

            var place = await Places.GetPlace(prediction.Place_ID, Settings.GooglePlacesApiKey);

            if (place != null)
            {
                ViewModel.SelectedLocationPlaceId = prediction.Place_ID;
                ViewModel.SelectedLocationCoordinates = $"{place.Latitude};{place.Longitude}";
                ViewModel.SelectedLocationTitle = prediction.Description;
            }
            await ViewModel.ExecuteSaveCommand();
        }
    }
}
