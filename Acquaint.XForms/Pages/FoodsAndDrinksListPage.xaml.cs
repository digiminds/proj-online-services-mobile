﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acquaint.XForms.ViewModels;
using OnlineServices.Models;
using OnlineServices.Util;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Acquaint.XForms.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FoodsAndDrinksListPage : ContentPage
    {
        protected SubCategoryListViewModel ViewModel => BindingContext as SubCategoryListViewModel;
        public bool GridInitialized = false;

        public FoodsAndDrinksListPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (!Settings.IsLocationSelected && Device.RuntimePlatform == Device.iOS)
            {
                var navPage = new NavigationPage(new ChangeLocationPage()
                {
                    Title = "Choose Location",
                    BindingContext = new SettingsViewModel()
                });

                await Navigation.PushModalAsync(navPage);
            }
            else
            {
                await ViewModel.ExecuteLoadSubCategoriesCommand();

                if (!GridInitialized)
                {
                    InitializeGrid();
                    GridInitialized = true;
                }
            }
        }

        void InitializeGrid()
        {
            var categories = ViewModel.SubCategories;
            FoodsAndDrinksGrid.RowDefinitions = new RowDefinitionCollection();
            FoodsAndDrinksGrid.ColumnDefinitions = new ColumnDefinitionCollection();
            FoodsAndDrinksGrid.Children.Clear();

            FoodsAndDrinksGrid.ColumnSpacing = 1;
            FoodsAndDrinksGrid.RowSpacing = 1;
            FoodsAndDrinksGrid.RowDefinitions.Add(new RowDefinition());
            FoodsAndDrinksGrid.ColumnDefinitions.Add(new ColumnDefinition());

            var row = 0;
            foreach (var category in categories)
            {
                var itemTaped = new TapGestureRecognizer
                {
                    Command = new Command(() => ItemTapped(category))
                };

                if (Device.RuntimePlatform == Device.Android)
                {
                    var image = new Image
                    {
                        Source = category.IconUrl,
                        HorizontalOptions = LayoutOptions.Fill,
                        VerticalOptions = LayoutOptions.Center,
                        BackgroundColor = Color.Gray,
                        Aspect = Aspect.AspectFill
                    };

                    var view = new StackLayout()
                    {
                        Children =
                    {
                        image
                    }
                    };

                    var frame = new Frame
                    {
                        Padding = new Thickness(0, 0),
                        Margin = new Thickness(10, 5, 10, 5),
                        CornerRadius = 5,
                        BackgroundColor = Color.Gray,
                        Content = view,
                        HorizontalOptions = LayoutOptions.Fill,
                        VerticalOptions = LayoutOptions.Fill,
                        //HeightRequest = 150,
                        HasShadow = true,
                        OutlineColor = Color.Gray
                    };
                    frame.GestureRecognizers.Add(itemTaped);

                    FoodsAndDrinksGrid.Children.Add(frame, 0, row); 
                }
                else if (Device.RuntimePlatform == Device.iOS)
                {
                    var image = new Image
                    {
                        Source = category.IconUrl,
                        Margin = new Thickness(10, 5, 10, 5),
                        HorizontalOptions = LayoutOptions.Fill,
                        VerticalOptions = LayoutOptions.Fill,
                        BackgroundColor = Color.Gray,
                        Aspect = Aspect.AspectFill
                    };
                    image.GestureRecognizers.Add(itemTaped);
                    FoodsAndDrinksGrid.Children.Add(image, 0, row);
                }
                row++;
            }
        }

        public void RefreshSubCategories(object sender, EventArgs e)
        {
            Refresh();
        }

        private async Task Refresh()
        {
            await ViewModel.ExecuteRefreshSubCategoriesCommand();
            InitializeGrid();
        }

        void ItemTapped(SubCategory item)
        {
            Navigation.PushAsync(new ServiceProviderListPage()
            {
                Title = $"{item.Title} - {item.ArabicTitle}",
                BindingContext = new ServiceProviderListViewModel(item)
            });
        }
    }
}