﻿using Acquaint.XForms.ViewModels;
using OnlineServices.Models;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlineServices.Models.DTO;

using Xamarin.Forms;
using OnlineServices.Models.Filters;
using OnlineServices.Util;

namespace Acquaint.XForms.Pages
{
    public partial class ServiceProviderListPage : ContentPage
    {
        protected ServiceProviderListViewModel ViewModel => BindingContext as ServiceProviderListViewModel;
        private Command _AvailableOnlyCommand;

        public ServiceProviderListPage()
        {
            InitializeComponent();

            if (Device.RuntimePlatform != Device.WinPhone)
            {
                ToolbarItems.Remove(refreshToolbarItem);
            }
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await ViewModel.ExecuteLoadServiceProvidersCommand();

            if (Settings.IsUseCurrentLocation)
            {
                if (CrossGeolocator.IsSupported)
                {
                    var locator = CrossGeolocator.Current;
                    if (locator != null)
                    {
                        if (!locator.IsGeolocationEnabled || !locator.IsGeolocationAvailable)
                            await DisplayAlert("Location", "Location on device is disabled! Kindly enable location to get the nearest provider", "Ok");
                    }
                }
            }

            searchByNameAndroid.Placeholder = "Search...";
            searchByNameAndroid.TextChanged += async (object sender, TextChangedEventArgs e) =>
            {
                if (string.IsNullOrEmpty(e.NewTextValue))
                {
                    await ViewModel.ExecuteLoadServiceProvidersCommand();
                }
            };

            availabilityToolbarItem.Command = AvailableOnlyCommand;
        }

        public Command AvailableOnlyCommand
        {
            get { return _AvailableOnlyCommand ?? (_AvailableOnlyCommand = new Command(async () => await ExecuteAvailableOnlyCommand())); }
        }

        private async Task ExecuteAvailableOnlyCommand()
        {
            AvailableOnlyCommand.ChangeCanExecute();
            ViewModel.ShowAvailableOnly = !ViewModel.ShowAvailableOnly;
            if (ViewModel.ShowAvailableOnly)
                availabilityToolbarItem.Icon = "icon_available_white.png";
            else
                availabilityToolbarItem.Icon = "icon_available_grey.png";

            var filter = new ServiceProviderFilter
            {
                AvailableOnly = ViewModel.ShowAvailableOnly
            };
            await ViewModel.FetchServiceProviders(filter);
            AvailableOnlyCommand.ChangeCanExecute();
        }

        void ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Navigation.PushAsync(new ServiceProviderDetailPage()
            {
                Title = ((ServiceProviderDTO)e.Item).FullName,
                BindingContext = new ServiceProviderDetailViewModel((ServiceProviderDTO)e.Item)
            });

            ((ListView)sender).SelectedItem = null;
        }
    }
}
