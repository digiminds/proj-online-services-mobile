﻿using Acquaint.XForms.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Acquaint.XForms.Pages
{
    public partial class ServiceProviderDetailPage : ContentPage
    {
        protected ServiceProviderDetailViewModel ViewModel => BindingContext as ServiceProviderDetailViewModel;
        
        public ServiceProviderDetailPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await ViewModel.ExecuteFetchServiceProvider();

            //SetupMap();

            MessagingCenter.Subscribe<string>(this, "OpenPhoneDecisionPopup", async (value) =>
                {
                    //the value parameter will now equal the value sent from the view model!
                    var action = "";
                    if (ViewModel.HasPhoneNumber)
                    {
                        action = await DisplayActionSheet("Contact Provider / تواصل ", "Cancel", null, "Mobile",
                            "WhatsApp", "Phone", "SMS");
                    }
                    else
                    {
                        action = await DisplayActionSheet("Contact Provider / تواصل ", "Cancel", null, "Mobile",
                            "WhatsApp", "SMS");
                    }

                    if (string.IsNullOrEmpty(action) || action == "Cancel")
                        return;

                    if (action == "Mobile")
                    {
                        ViewModel.ExecuteDialNumberCommand(ViewModel.SelectedProvider.MobileNumber);
                    }
                    else if (action == "WhatsApp")
                    {
                        ViewModel.ExecuteOpenWhatsappCommand();
                    }
                    else if (action == "Phone")
                    {
                        ViewModel.ExecuteDialNumberCommand(ViewModel.SelectedProvider.PhoneNumber);
                    }
                    else if (action == "SMS")
                    {
                        ViewModel.ExecuteMessageNumberCommand();
                    }
                }
            );
        }

        //void SetupMap()
        //{
        //    if (ViewModel.HasAddress)
        //    {
        //        SelectedProviderMap.IsVisible = false;

        //        // set to a default position
        //        var selectedProvider = ViewModel.SelectedProvider;
        //        Position position = new Position(selectedProvider.Latitude, selectedProvider.Longitude);

        //        // if lat and lon are both 0, then it's assumed that position acquisition failed
        //        if (position.Latitude == 0 && position.Longitude == 0)
        //            return;

        //        var pin = new Pin()
        //        {
        //            Type = PinType.Generic,
        //            Position = position,
        //            Label = ViewModel.SelectedProvider.FullName,
        //            Address = ViewModel.SelectedProvider.LocationDescription
        //        };

        //        SelectedProviderMap.Pins.Clear();
        //        SelectedProviderMap.Pins.Add(pin);
        //        SelectedProviderMap.MoveToRegion(MapSpan.FromCenterAndRadius(position, Distance.FromMiles(10)));

        //        SelectedProviderMap.IsVisible = true;
        //    }
        //}
    }
}
