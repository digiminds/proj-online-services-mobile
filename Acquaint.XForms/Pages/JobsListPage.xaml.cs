﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acquaint.XForms.ViewModels;
using OnlineServices.Models.DTO;
using OnlineServices.Util;
using Plugin.Geolocator;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Acquaint.XForms.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class JobsListPage : ContentPage
    {
        protected ServiceProviderListViewModel ViewModel => BindingContext as ServiceProviderListViewModel;
        public JobsListPage()
        {
            InitializeComponent();
            if (Device.RuntimePlatform != Device.WinPhone)
            {
                ToolbarItems.Remove(refreshToolbarItem);
            }
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await ViewModel.ExecuteLoadServiceProvidersCommand();

            if (Settings.IsUseCurrentLocation)
            {
                if (CrossGeolocator.IsSupported)
                {
                    var locator = CrossGeolocator.Current;
                    if (locator != null)
                    {
                        if (!locator.IsGeolocationEnabled || !locator.IsGeolocationAvailable)
                            await DisplayAlert("Location", "Location on device is disabled! Kindly enable location to get the nearest provider", "Ok");
                    }
                }
            }

            searchByName.Placeholder = "Search...";
            searchByName.TextChanged += async (object sender, TextChangedEventArgs e) =>
            {
                if (string.IsNullOrEmpty(e.NewTextValue))
                {
                    await ViewModel.ExecuteLoadServiceProvidersCommand();
                }
            };
        }

        void ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Navigation.PushAsync(new JobsDetailPage()
            {
                Title = ((ServiceProviderDTO)e.Item).FullName,
                BindingContext = new ServiceProviderDetailViewModel((ServiceProviderDTO)e.Item)
            });

            ((ListView)sender).SelectedItem = null;
        }
    }
}