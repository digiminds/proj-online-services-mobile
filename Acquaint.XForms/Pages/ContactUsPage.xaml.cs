﻿using Acquaint.XForms.ViewModels;
using FormsToolkit;
using OnlineServices.Abstractions;
using Plugin.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlineServices.Data;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Acquaint.XForms.Pages
{
    public partial class ContactUsPage : ContentPage
    {
        protected ContactUsViewModel ViewModel => BindingContext as ContactUsViewModel;
        public ContactUsPage()
        {
            InitializeComponent();
            BindingContext = new ContactUsViewModel();
        }

        private void sendEmail_Clicked(object sender, EventArgs e)
        {
            ExecuteSendMessageCommand();
        }
        public async Task ExecuteSendMessageCommand()
        {
            if (string.IsNullOrWhiteSpace(Email.Text))
                return;
            
            string messageContent = "Hello,\n" +
                                    "I would like to register my name in BADDE application\n\n\n";
            messageContent += "Name: " + FullName.Text + "\n";
            messageContent += "Phone: " + Phone.Text + "\n";
            messageContent += "Email: " + Email.Text + "\n";
            messageContent += "Line of Work: " + ServiceCategory.Text + "\n";
            messageContent += "Message Content:\n" + Message.Text;

            Device.OpenUri(
                new Uri($"https://wa.me/+96171291255?text=" + System.Net.WebUtility.UrlEncode(messageContent)));

            var restClient = new RestService<string>("EmailSender");
            await restClient.PostPayload("SendEmail", new
            {
                Subject = "New Request",
                MessageBody = messageContent
            });

            await Navigation.PopToRootAsync(true);

        }
    }
}