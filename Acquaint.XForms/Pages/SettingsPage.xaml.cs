﻿using System;
using System.Collections.Generic;
using FormsToolkit;
using Xamarin.Forms;
using Acquaint.XForms.Pages;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Xamarin.Forms.Maps;

namespace Acquaint.XForms
{
    public partial class SettingsPage : ContentPage
    {
        protected SettingsViewModel ViewModel => BindingContext as SettingsViewModel;

        public SettingsPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        public void ShowChangeLocation(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ChangeLocationPage()
            {
                Title = "Change Location",
                BindingContext = ViewModel
            });
        }

        public void Register(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ContactUsPage()
            {
                Title = "Register Your Service",
                BindingContext = new ContactUsPage()
            });
        }

        public void AboutUs(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AboutUsPage()
            {
                Title = "About Us",
                BindingContext = new AboutUsPage()
            });
        }
    }
}

