﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OnlineServices.Util;
using Xamarin.Forms;
using Acquaint.XForms.Pages;

namespace Acquaint.XForms
{
    public partial class SetupPage : ContentPage
    {
        protected SettingsViewModel ViewModel => BindingContext as SettingsViewModel;
        public SetupPage()
        {
            InitializeComponent();
        }        

        public void ShowChangeLocation(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ChangeLocationPage()
            {
                Title = "Choose Location",
                BindingContext = ViewModel
            });
        }

        protected override bool OnBackButtonPressed()
        {
            // disable back button, so that the user is forced to enter a DataPartitionPhrase
            return true;
        }
    }
}

