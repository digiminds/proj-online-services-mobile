﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acquaint.XForms.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Acquaint.XForms.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class JobsDetailPage : ContentPage
    {
        protected ServiceProviderDetailViewModel ViewModel => BindingContext as ServiceProviderDetailViewModel;

        public JobsDetailPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await ViewModel.ExecuteFetchServiceProvider();

            //SetupMap();
        }
    }
}