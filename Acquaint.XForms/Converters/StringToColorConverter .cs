﻿using OnlineServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Acquaint.XForms.Converters
{
    public class StringToColorConverter : IValueConverter
    {

        #region IValueConverter implementation

        public object Convert(object value, Type targetType = null, object parameter = null, System.Globalization.CultureInfo culture=null )
        {
           // string s = (string)value;
            SubCategory cat = (SubCategory)value;
            switch (cat.Color.ToString())
            {
                case "AliceBlue":
                    return Color.AliceBlue;
                case "Beige":
                    return Color.Beige;
                case "Chartreuse":
                    return Color.Chartreuse;
                case "Coral":
                    return Color.Coral;
                case "DarkKhaki":
                    return Color.DarkKhaki;
                case "Firebrick":
                    return Color.Firebrick;
                default:
                    return Color.Black;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
