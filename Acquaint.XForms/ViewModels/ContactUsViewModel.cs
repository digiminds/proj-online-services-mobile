﻿using FormsToolkit;
using MvvmHelpers;
using OnlineServices.Abstractions;
using OnlineServices.Models.Models;
using Plugin.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Acquaint.XForms.ViewModels
{
    public class ContactUsViewModel : BaseNavigationViewModel
    {
 
        #region Fields
        // Observable<ContactUsForm> _ContactForm;

        ContactUsForm enteredContactForm;
        Command _CancelCommand;
        Command _SendMessageCommand;
        readonly ICapabilityService _CapabilityService;

        #endregion

        #region Properties
        public ICapabilityService CapabilityService => _CapabilityService;

        //public List<ContactUsForm> ContactUsForm
        //{
        //    get { return _ContactForm ?? (_ContactForm = new List<ContactUsForm>()); }
        //    set
        //    {
        //        _ContactForm = value;
        //        OnPropertyChanged("ContactUsForm");
        //    }
        //}
        public ContactUsForm Model
        {
            get { return _Model; }
            set
            {
                _Model = Model;
                OnPropertyChanged("Model");
            }
        }


        #endregion

        #region ctor
        public ContactUsViewModel()
        {
            _CapabilityService = DependencyService.Get<ICapabilityService>();
            Model = new ContactUsForm();


        }
        #endregion

        #region Command

        public Command CancelCommand => _CancelCommand ?? (_CancelCommand = new Command(async () => await ExecuteCancelCommand()));
        public Command SendMessageCommand
        {
            get
            {
                return _SendMessageCommand ?? (_SendMessageCommand = new Command(ExecuteSendMessageCommand));
            }
        }


        #endregion

        #region Command Execution

        async Task ExecuteCancelCommand()
        {
            await PopModalAsync();
        }

        public void ExecuteSendMessageCommand()
        {
            if (string.IsNullOrWhiteSpace(Model.Email))
                return;

            if (CapabilityService.CanSendEmail)
            {
                var emailTask = CrossMessaging.Current.EmailMessenger;
                //test scenarios
                if (emailTask.CanSendEmail)
                    emailTask.SendEmail("gaellebs@outlook.com","Test Subject", Model.MessageContent);
            }
            else
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = "Not Supported",
                    Message = "Email composition is not supported.",
                    Cancel = "OK"
                });
            }
        }

        #endregion

        #region Private Properties

        private ContactUsForm _Model;

        #endregion
    }
}
