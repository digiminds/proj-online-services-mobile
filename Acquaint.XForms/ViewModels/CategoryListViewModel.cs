﻿using Acquaint.XForms.Pages;
using Microsoft.Practices.ServiceLocation;
using MvvmHelpers;
using OnlineServices.Abstractions;
using OnlineServices.ModelContracts;
using OnlineServices.Models;
using OnlineServices.Models.Filters;
using OnlineServices.Util;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Acquaint.XForms.ViewModels
{
    public class CategoryListViewModel : BaseNavigationViewModel
    {
        #region Fields

        private IDataSource<Category, CategoryFilter> _dataSource;
        private List<Category> _categories;

        private Command _loadCategoriesCommand;
        private Command _refreshCategoriesCommand;
        #endregion

        #region Properties
        public List<Category> Categories
        {
            get { return _categories ?? (_categories = new List<Category>()); }
            set
            {
                _categories = value;
                OnPropertyChanged("Categories");
            }
        }
        #endregion

        #region Ctor
        public CategoryListViewModel()
        {
            SetDataSource();
        }
        #endregion

        #region Commands
        public Command LoadCategoriesCommand
        {
            get { return _loadCategoriesCommand ?? (_loadCategoriesCommand = new Command(async () => await ExecuteLoadCategoriesCommand())); }
        }

        public Command RefreshCategoriesCommand
        {
            get { return _refreshCategoriesCommand ?? (_refreshCategoriesCommand = new Command(async () => await ExecuteRefreshCategoriesCommand())); }
        }
        #endregion

        #region Command Execution
        public async Task ExecuteLoadCategoriesCommand()
        {
            LoadCategoriesCommand.ChangeCanExecute();

            // set the data source on each load, because we don't know if the data source may have been updated between page loads
            SetDataSource();

            if (Settings.LocalDataResetIsRequested)
                _categories.Clear();

            if (Categories.Count < 1 || !Settings.DataIsSeeded || Settings.ClearImageCacheIsRequested)
                await FetchCategories();

            LoadCategoriesCommand.ChangeCanExecute();
        }

        public async Task ExecuteRefreshCategoriesCommand()
        {
            RefreshCategoriesCommand.ChangeCanExecute();

            await FetchCategories();

            RefreshCategoriesCommand.ChangeCanExecute();
        }
        #endregion

        #region Private Methods
        private void SetDataSource()
        {
            _dataSource = ServiceLocator.Current.GetInstance<IDataSource<Category, CategoryFilter>>();
        }

        private async Task FetchCategories()
        {
            IsBusy = true;

            Categories = new List<Category>(await _dataSource.GetItems());

            IsBusy = false;
        }
        #endregion
    }
}
