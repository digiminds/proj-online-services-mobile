﻿using Acquaint.XForms.Pages;
using FormsToolkit;
using Microsoft.Practices.ServiceLocation;
using MvvmHelpers;
using OnlineServices.Abstractions;
using OnlineServices.Models;
using OnlineServices.Models.DTO;
using OnlineServices.Models.Filters;
using OnlineServices.Util;
using Plugin.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Acquaint.XForms.ViewModels
{
    public class ServiceProviderListViewModel : BaseNavigationViewModel
    {
        #region Fields
        private bool _noProvidersFound;
        private bool _isServiceProvider = true;
        private bool _isJob = false;
        IDataSource<ServiceProviderDTO, ServiceProviderFilter> _DataSource;
        List<ServiceProviderDTO> _ServiceProviders;
        readonly ICapabilityService _CapabilityService;

        SubCategory SelectedSubCategory;
        bool _showAvailableOnly = false;

        Command _LoadServiceProvidersCommand;
        Command _RefreshServiceProvidersCommand;
        Command _ShowChangeLocationCommand;
        Command _SearchCommand;
        Command _DialNumberCommand;
        Command _MessageNumberCommand;
        Command _EmailCommand;
        #endregion

        #region Properties
        public List<ServiceProviderDTO> ServiceProviders
        {
            get { return _ServiceProviders ?? (_ServiceProviders = new List<ServiceProviderDTO>()); }
            set
            {
                _ServiceProviders = value;
                OnPropertyChanged("ServiceProviders");
            }
        }

        public bool ShowAvailableOnly
        {
            get { return _showAvailableOnly; }
            set
            {
                _showAvailableOnly = value;
                OnPropertyChanged("ShowAvailableOnly");
            }
        }

        public bool NoProvidersFound
        {
            get { return _noProvidersFound; }
            set
            {
                _noProvidersFound = value;
                OnPropertyChanged("NoProvidersFound");
            }
        }

        public bool IsServiceProvider
        {
            get { return _isServiceProvider; }
            set
            {
                _isServiceProvider = value;
                OnPropertyChanged("IsServiceProvider");
            }
        }

        public bool IsJob
        {
            get { return _isJob; }
            set
            {
                _isJob = value;
                OnPropertyChanged("IsJob");
            }
        }

        #endregion

        #region Ctor
        public ServiceProviderListViewModel(SubCategory category)
        {
            _CapabilityService = DependencyService.Get<ICapabilityService>();
            SelectedSubCategory = category;
            IsServiceProvider = !SelectedSubCategory.Name.Equals("job");
            IsJob = SelectedSubCategory.Name.Equals("job");
            SetDataSource();
        }
        #endregion

        #region Commands
        public Command LoadServiceProvidersCommand
        {
            get { return _LoadServiceProvidersCommand ?? (_LoadServiceProvidersCommand = new Command(async () => await ExecuteLoadServiceProvidersCommand())); }
        }

        public Command RefreshServiceProvidersCommand
        {
            get { return _RefreshServiceProvidersCommand ?? (_RefreshServiceProvidersCommand = new Command(async () => await ExecuteRefreshServiceProvidersCommand())); }
        }

        public Command ShowChangeLocationCommand
        {
            get { return _ShowChangeLocationCommand ?? (_ShowChangeLocationCommand = new Command(async () => await ExecuteShowChangeLocationCommand())); }
        }

        public Command SearchCommand
        {
            get { return _SearchCommand ?? (_SearchCommand = new Command(async (object criteria) => await ExecuteSearchCommand(criteria.ToString()))); }
        }

        public Command DialNumberCommand
        {
            get
            {
                return _DialNumberCommand ??
                (_DialNumberCommand = new Command((parameter) =>
                        ExecuteDialNumberCommand((Guid)parameter)));
            }
        }

        public Command MessageNumberCommand
        {
            get
            {
                return _MessageNumberCommand ??
                (_MessageNumberCommand = new Command((parameter) =>
                        ExecuteMessageNumberCommand((Guid)parameter)));
            }
        }

        public Command EmailCommand
        {
            get
            {
                return _EmailCommand ??
                (_EmailCommand = new Command((parameter) =>
                        ExecuteEmailCommand((Guid)parameter)));
            }
        }
        #endregion

        #region Command Execution
        public async Task ExecuteLoadServiceProvidersCommand()
        {
            LoadServiceProvidersCommand.ChangeCanExecute();

            // set the data source on each load, because we don't know if the data source may have been updated between page loads
            SetDataSource();

            if (Settings.LocalDataResetIsRequested)
                _ServiceProviders.Clear();

            if (ServiceProviders.Count < 1 || !Settings.DataIsSeeded || Settings.ClearImageCacheIsRequested)
            {
                var filter = new ServiceProviderFilter
                {
                    AvailableOnly = ShowAvailableOnly
                };
                await FetchServiceProviders(filter);
            }

            LoadServiceProvidersCommand.ChangeCanExecute();
        }

        public async Task ExecuteRefreshServiceProvidersCommand()
        {
            RefreshServiceProvidersCommand.ChangeCanExecute();
            var filter = new ServiceProviderFilter
            {
                AvailableOnly = ShowAvailableOnly
            };
            await FetchServiceProviders(filter);

            RefreshServiceProvidersCommand.ChangeCanExecute();
        }

        public async Task ExecuteShowChangeLocationCommand()
        {
            ShowChangeLocationCommand.ChangeCanExecute();

            var navPage = new NavigationPage(
                new ChangeLocationPage() { BindingContext = new SettingsViewModel() });

            await PushModalAsync(navPage);

            ShowChangeLocationCommand.ChangeCanExecute();
        }

        public async Task ExecuteSearchCommand(string criteria)
        {
            SearchCommand.ChangeCanExecute();

            // set the data source on each load, because we don't know if the data source may have been updated between page loads
            SetDataSource();

            if (Settings.LocalDataResetIsRequested)
                _ServiceProviders.Clear();

            if (_ServiceProviders.Count < 1 || !Settings.DataIsSeeded || Settings.ClearImageCacheIsRequested)
            {
                var filter = new ServiceProviderFilter
                {
                    AvailableOnly = ShowAvailableOnly,
                    Criteria = criteria
                };
                await FetchServiceProviders(filter);
            }

            SearchCommand.ChangeCanExecute();
        }

        void ExecuteDialNumberCommand(Guid providerId)
        {
            DialNumberCommand.ChangeCanExecute();
            if (providerId.Equals(Guid.Empty))
                return;

            var provider = _ServiceProviders.SingleOrDefault(c => c.Id == providerId);

            if (provider == null)
                return;

            if (_CapabilityService.CanMakeCalls)
            {
                var phoneCallTask = CrossMessaging.Current.PhoneDialer;
                if (phoneCallTask.CanMakePhoneCall)
                    phoneCallTask.MakePhoneCall(provider.MobileNumber.SanitizePhoneNumber());
            }
            else
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = "Not Supported",
                    Message = "Phone calls are not supported",
                    Cancel = "OK"
                });
            }
            DialNumberCommand.ChangeCanExecute();
        }

        void ExecuteMessageNumberCommand(Guid providerId)
        {
            MessageNumberCommand.ChangeCanExecute();
            if (providerId.Equals(Guid.Empty))
                return;

            var provider = _ServiceProviders.SingleOrDefault(c => c.Id == providerId);

            if (provider == null)
                return;

            if (_CapabilityService.CanSendMessages)
            {
                var messageTask = CrossMessaging.Current.SmsMessenger;
                if (messageTask.CanSendSms)
                    messageTask.SendSms(provider.MobileNumber.SanitizePhoneNumber());
            }
            else
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = "Not Supported",
                    Message = "Messaging is not supported",
                    Cancel = "OK"
                });
            }
            DialNumberCommand.ChangeCanExecute();
        }

        void ExecuteEmailCommand(Guid providerId)
        {
            EmailCommand.ChangeCanExecute();
            if (providerId.Equals(Guid.Empty))
                return;

            var provider = _ServiceProviders.SingleOrDefault(c => c.Id == providerId);

            if (provider == null)
                return;

            if (_CapabilityService.CanSendEmail)
            {
                var emailTask = CrossMessaging.Current.EmailMessenger;
                if (emailTask.CanSendEmail)
                    emailTask.SendEmail(provider.Email);
            }
            else
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = "Not Supported",
                    Message = "Email composition is not supported",
                    Cancel = "OK"
                });
            }
            EmailCommand.ChangeCanExecute();
        }
        #endregion

        #region Private Methods
        private void SetDataSource()
        {
            _DataSource = ServiceLocator.Current.GetInstance<IDataSource<ServiceProviderDTO, ServiceProviderFilter>>();
        }

        public async Task FetchServiceProviders(ServiceProviderFilter filter)
        {
            IsBusy = true;
            NoProvidersFound = false;
            filter = filter ?? new ServiceProviderFilter();
            filter.ParentId = SelectedSubCategory.Id;
            ServiceProviders = new List<ServiceProviderDTO>(await _DataSource.GetItems(filter));

            // ensuring that this flag is reset
            Settings.ClearImageCacheIsRequested = false;

            if (ServiceProviders.Count == 0)
            {                
                NoProvidersFound = true;
            }
            
            IsBusy = false;
        }
        #endregion
    }
}
