﻿using System;
using System.Threading.Tasks;
using OnlineServices.Util;
using FFImageLoading;
using FFImageLoading.Cache;
using FormsToolkit;
using Xamarin.Forms;

namespace Acquaint.XForms
{
    public class SettingsViewModel : BaseNavigationViewModel
    {
        #region Fields
        private bool _UseCurrentLocation;
        private string _SelectedLocationTitle;

        Command _CancelCommand;
        Command _SaveCommand;
        Command _UseCurrentLocationCommand;
        #endregion

        #region Properties
        public string SelectedLocationPlaceId { get; set; }
        public string SelectedLocationCoordinates { get; set; }
        public bool CanSave
        {
            get
            {
                return UseCurrentLocation || (!UseCurrentLocation && !string.IsNullOrEmpty(SelectedLocationPlaceId));
            }
        }
        public bool UseCurrentLocation
        {
            get
            {
                return _UseCurrentLocation;
            }
            set
            {
                if (value)
                    SelectedLocationPlaceId = null;

                SetProperty(ref _UseCurrentLocation, value);
                OnPropertyChanged(nameof(UseCurrentLocation));
            }
        }
        public string SelectedLocationTitle
        {
            get
            {
                return _SelectedLocationTitle ?? (_SelectedLocationTitle = Settings.UserSelectedLocationTitle);
            }
            set
            {
                SetProperty(ref _SelectedLocationTitle, value);
                OnPropertyChanged(nameof(SelectedLocationTitle));
            }
        }
        #endregion

        #region Ctor
        public SettingsViewModel()
        {
            _UseCurrentLocation = Settings.IsUseCurrentLocation;
        }
        #endregion

        #region Commands
        public Command SaveCommand => _SaveCommand ?? (_SaveCommand = new Command(async () => await ExecuteSaveCommand()));
        public Command CancelCommand => _CancelCommand ?? (_CancelCommand = new Command(async () => await ExecuteCancelCommand()));
        public Command UseCurrentLocationCommand => _UseCurrentLocationCommand ?? (_UseCurrentLocationCommand = new Command(async () => await ExecuteSaveCommand(true)));
        #endregion

        #region Command Execution
        public async Task ExecuteSaveCommand(bool useCurrentLocation = false)
        {
            UseCurrentLocation = useCurrentLocation;
            if (CanSave)
            {
                if (useCurrentLocation)
                {
                    var userCoor = await LocationUtility.GetCurrentPosition(1000);
                    Settings.UserLocation = "UseCurrentLocation";
                    Settings.UserSelectedLocationPlaceId = string.Empty;
                    Settings.UserSelectedLocationCoordinates = userCoor != null ? $"{userCoor.Latitude};{userCoor.Longitude}" : string.Empty;
                    Settings.UserSelectedLocationTitle = "Current Location";
                }
                else
                {
                    Settings.UserLocation = "UseSelectedLocation";
                    Settings.UserSelectedLocationPlaceId = SelectedLocationPlaceId;
                    Settings.UserSelectedLocationCoordinates = SelectedLocationCoordinates;
                    Settings.UserSelectedLocationTitle = SelectedLocationTitle;
                }
                await PopModalAsync();
            }
        }

        async Task ExecuteCancelCommand()
        {
            await PopModalAsync();
        }
        #endregion
    }
}

