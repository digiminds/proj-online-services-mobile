﻿using FormsToolkit;
using Microsoft.Practices.ServiceLocation;
using OnlineServices.Abstractions;
using OnlineServices.Models.DTO;
using OnlineServices.Models.Filters;
using OnlineServices.Util;
using Plugin.ExternalMaps;
using Plugin.ExternalMaps.Abstractions;
using Plugin.Messaging;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Acquaint.XForms.ViewModels
{
    public class ServiceProviderDetailViewModel : BaseNavigationViewModel
    {
        #region Fields
        IDataSource<ServiceProviderDTO, ServiceProviderFilter> _DataSource;

        readonly ICapabilityService _CapabilityService;

        readonly Geocoder _Geocoder;

        private Command _fetchServiceProviderCommand;
        private Command _openPhoneDecisionPopupCommand;
        private Command _dialMobileNumberCommand;
        private Command _dialPhoneNumberCommand;
        private Command _whatsappNumberCommand;
        private Command _messageNumberCommand;
        private Command _emailCommand;
        private Command _getDirectionsCommand;
        private Command _openFacebookCommand;
        private Command _openInstagramCommand;
        #endregion

        #region Ctor
        public ServiceProviderDetailViewModel(ServiceProviderDTO serviceProvider)
        {
            _CapabilityService = DependencyService.Get<ICapabilityService>();
            _Geocoder = new Geocoder();
            SelectedProvider = serviceProvider;
            SetDataSource();
        }
        #endregion

        #region Properties
        public ServiceProviderDTO SelectedProvider { private set; get; }

        public ICapabilityService CapabilityService => _CapabilityService;

        public bool HasAvailability => !string.IsNullOrWhiteSpace(SelectedProvider?.AvailabilityString);

        public bool HasPhoto => !string.IsNullOrWhiteSpace(SelectedProvider?.ProfilePhotoUrl);

        public bool HasEmailAddress => !string.IsNullOrWhiteSpace(SelectedProvider?.Email);

        public bool HasPhoneNumber => !string.IsNullOrWhiteSpace(SelectedProvider?.PhoneNumber);

        public bool HasMobileNumber => !string.IsNullOrWhiteSpace(SelectedProvider?.MobileNumber);

        public bool HasMobileAndPhoneNumber => HasPhoneNumber && HasMobileNumber;

        public bool HasRate => SelectedProvider != null && SelectedProvider.Rate > 0;

        public bool HasDescription => !string.IsNullOrWhiteSpace(SelectedProvider?.Description);

        public bool HasFacebook => !string.IsNullOrWhiteSpace(SelectedProvider?.Facebook);

        public bool HasInstagram => !string.IsNullOrWhiteSpace(SelectedProvider?.Instagram);

        public bool HasAddress => SelectedProvider != null && SelectedProvider.Longitude != 0 && SelectedProvider.Latitude != 0;        

        public string EmailIcon => HasEmailAddress ? "button_email.png" : "button_email_disabled.png";
        public string MobileNumberIcon => HasMobileNumber ? "button_mobile_call.png" : "button_mobile_call_disabled.png";
        public string PhoneNumberIcon => HasPhoneNumber ? "button_call.png" : "button_call_disabled.png";
        public string MessageIcon => HasMobileNumber ? "button_whatsapp.png" : "button_whatsapp_disabled.png";
        public string DirectionIcon => HasAddress ? "button_directions.png" : "button_directions_disabled.png";
        public string FacebookIcon => HasFacebook ? "button_facebook.png" : "button_facebook_disabled.png";
        public string InstagramIcon => HasInstagram ? "button_instagram.png" : "button_instagram_disabled.png";
        #endregion

        #region Public Method
        public void SetupMap()
        {
            if (HasAddress)
            {
                MessagingService.Current.SendMessage(MessageKeys.SetupMap);
            }
        }
        #endregion

        #region Private Methods
        private void SetDataSource()
        {
            _DataSource = ServiceLocator.Current.GetInstance<IDataSource<ServiceProviderDTO, ServiceProviderFilter>>();
        }
        #endregion

        #region Commands
        public Command FetchServiceProviderCommand
        {
            get
            {
                return _fetchServiceProviderCommand ?? (_fetchServiceProviderCommand = new Command(async () => await ExecuteFetchServiceProvider()));
            }
        }

        public Command WhatsappNumberCommand => _whatsappNumberCommand ?? (_whatsappNumberCommand =
                                                            new Command(ExecuteOpenWhatsappCommand));

        public Command OpenPhoneDecisionPopupCommand => _openPhoneDecisionPopupCommand ?? (_openPhoneDecisionPopupCommand =
                                                            new Command(ExecuteOpenPhoneDecisionPopupCommand));

        public Command DialMobileNumberCommand
        {
            get
            {
                return _dialMobileNumberCommand ?? (_dialMobileNumberCommand = new Command(() => ExecuteDialNumberCommand(SelectedProvider.MobileNumber)));
            }
        }

        public Command DialPhoneNumberCommand
        {
            get
            {
                return _dialPhoneNumberCommand ?? (_dialPhoneNumberCommand = new Command(() => ExecuteDialNumberCommand(SelectedProvider.PhoneNumber)));
            }
        }
        public Command MessageNumberCommand
        {
            get
            {
                return _messageNumberCommand ?? (_messageNumberCommand = new Command(ExecuteMessageNumberCommand));
            }
        }
        public Command EmailCommand
        {
            get
            {
                return _emailCommand ?? (_emailCommand = new Command(ExecuteEmailCommandCommand));
            }
        }

        public Command GetDirectionsCommand
        {
            get
            {
                return _getDirectionsCommand ?? (_getDirectionsCommand = new Command(async () => await ExecuteGetDirectionsCommand()));
            }
        }

        public Command OpenFacebookCommand
        {
            get
            {
                return _openFacebookCommand ?? (_openFacebookCommand = new Command(ExecuteOpenFacebookCommand));
            }
        }

        public Command OpenInstagramCommand
        {
            get
            {
                return _openInstagramCommand ?? (_openInstagramCommand = new Command(ExecuteOpenInstagramCommand));
            }
        }
        #endregion

        #region Commands Executions
        public async Task ExecuteFetchServiceProvider()
        {
            IsBusy = true;

            SelectedProvider = await _DataSource.GetItem(SelectedProvider.Id);

            IsBusy = false;
        }

        void ExecuteOpenPhoneDecisionPopupCommand()
        {
            MessagingCenter.Send(nameof(ServiceProviderDetailViewModel), "OpenPhoneDecisionPopup");
        }

        public void ExecuteDialNumberCommand(string number)
        {
            if (string.IsNullOrWhiteSpace(number))
                return;

            if (CapabilityService.CanMakeCalls)
            {
                var phoneCallTask = CrossMessaging.Current.PhoneDialer;
                if (phoneCallTask.CanMakePhoneCall)
                    phoneCallTask.MakePhoneCall(number.SanitizePhoneNumber());
            }
            else
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = "Not Supported",
                    Message = "Phone calls are not supported.",
                    Cancel = "OK"
                });
            }
        }

        public void ExecuteOpenWhatsappCommand()
        {
            if (!HasMobileNumber) return;

            var mobileNumber = !SelectedProvider.MobileNumber.Contains("+961")
                ? $"+961{SelectedProvider.MobileNumber}"
                : SelectedProvider.MobileNumber;

            Device.OpenUri(new Uri($"https://wa.me/{mobileNumber}"));
        }

        void ExecuteEmailCommandCommand()
        {
            if (!HasEmailAddress) return;

            if (CapabilityService.CanSendEmail)
            {
                var emailTask = CrossMessaging.Current.EmailMessenger;
                if (emailTask.CanSendEmail)
                    emailTask.SendEmail(SelectedProvider.Email);
            }
            else
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = "Not Supported",
                    Message = "Email composition is not supported.",
                    Cancel = "OK"
                });
            }
        }

        public void ExecuteMessageNumberCommand()
        {
            if (!HasMobileNumber) return;

            if (CapabilityService.CanSendMessages)
            {
                var messageTask = CrossMessaging.Current.SmsMessenger;
                if (messageTask.CanSendSms)
                    messageTask.SendSms(SelectedProvider.MobileNumber.SanitizePhoneNumber());
            }
            else
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = "Not Supported",
                    Message = "Messaging is not supported",
                    Cancel = "OK"
                });
            }
        }

        async Task ExecuteGetDirectionsCommand()
        {
            if (!HasAddress) return;
            var pin = new Pin {Position = new Position(SelectedProvider.Latitude, SelectedProvider.Longitude)};
            await CrossExternalMaps.Current.NavigateTo(pin.Label, pin.Position.Latitude, pin.Position.Longitude,
                NavigationType.Driving);
        }

        void ExecuteOpenFacebookCommand()
        {
            if (!HasFacebook) return;
            Device.OpenUri(new Uri(SelectedProvider.Facebook));
        }

        void ExecuteOpenInstagramCommand()
        {
            if (!HasInstagram) return;
            Device.OpenUri(new Uri(SelectedProvider.Instagram));
        }
        #endregion
    }
}
