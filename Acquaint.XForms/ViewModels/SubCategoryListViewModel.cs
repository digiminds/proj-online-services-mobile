﻿using Acquaint.XForms.Pages;
using Microsoft.Practices.ServiceLocation;
using MvvmHelpers;
using OnlineServices.Abstractions;
using OnlineServices.ModelContracts;
using OnlineServices.Models;
using OnlineServices.Models.Filters;
using OnlineServices.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Acquaint.XForms.ViewModels
{
    public class SubCategoryListViewModel : BaseNavigationViewModel
    {
        #region Fields
        private bool _noDataFound;
        private IEnumerable<SubCategory> _allSubCategories;

        private IDataSource<SubCategory, SubCategoryFilter> _dataSource;
        private List<SubCategory> _subCategories;
        private readonly Category _selectedCategory;

        private Command _loadSubCategoriesCommand;
        private Command _refreshSubCategoriesCommand;
        private Command _showChangeLocationCommand;
        private Command _searchCommand;
        private Command _contactUs;
        #endregion

        #region Properties

        public async Task<IEnumerable<SubCategory>> AllSubCategories(SubCategoryFilter filter)
        {
            _allSubCategories = await _dataSource.GetItems(filter);

            if (_allSubCategories == null || _allSubCategories != null && !_allSubCategories.Any())
                NoDataFound = true;
            else
            {
                NoDataFound = false;
            }
            return _allSubCategories;
        }

        public List<SubCategory> SubCategories
        {
            get { return _subCategories ?? (_subCategories = new List<SubCategory>()); }
            set
            {
                _subCategories = value;
                OnPropertyChanged("SubCategories");
            }
        }

        public bool NoDataFound
        {
            get { return _noDataFound; }
            set
            {
                _noDataFound = value;
                OnPropertyChanged("NoDataFound");
            }
        }
        #endregion

        #region Ctor
        public SubCategoryListViewModel()
        {
            SetDataSource();
        }

        public SubCategoryListViewModel(Category category)
        {
            this._selectedCategory = category;
            SetDataSource();
        }
        #endregion

        #region Commands
        public Command LoadSubCategoriesCommand
        {
            get { return _loadSubCategoriesCommand ?? (_loadSubCategoriesCommand = new Command(async () => await ExecuteLoadSubCategoriesCommand())); }
        }

        public Command RefreshSubCategoriesCommand
        {
            get { return _refreshSubCategoriesCommand ?? (_refreshSubCategoriesCommand = new Command(async () => await ExecuteRefreshSubCategoriesCommand())); }
        }

        public Command ShowChangeLocationCommand
        {
            get { return _showChangeLocationCommand ?? (_showChangeLocationCommand = new Command(async () => await ExecuteShowChangeLocationCommand())); }
        }

        public Command ContactUsCommand
        {
            get { return _contactUs ?? (_contactUs = new Command(async () => await ExecuteContactUsCommand())); }
        }

        public Command SearchCommand
        {
            get { return _searchCommand ?? (_searchCommand = new Command(async (object criteria) => await ExecuteSearchCommand(criteria.ToString()))); }
        }
        #endregion

        #region Command Execution
        public async Task ExecuteLoadSubCategoriesCommand()
        {
            LoadSubCategoriesCommand.ChangeCanExecute();

            // set the data source on each load, because we don't know if the data source may have been updated between page loads
            SetDataSource();

            if (Settings.LocalDataResetIsRequested)
                _subCategories.Clear();

            if (SubCategories.Count < 1 || !Settings.DataIsSeeded || Settings.ClearImageCacheIsRequested)
                await FetchSubCategories(new SubCategoryFilter());

            LoadSubCategoriesCommand.ChangeCanExecute();
        }

        public async Task ExecuteRefreshSubCategoriesCommand()
        {
            RefreshSubCategoriesCommand.ChangeCanExecute();

            await FetchSubCategories(new SubCategoryFilter(), true);

            RefreshSubCategoriesCommand.ChangeCanExecute();
        }

        public async Task ExecuteShowChangeLocationCommand()
        {
            ShowChangeLocationCommand.ChangeCanExecute();
            var navPage = new NavigationPage(
                new ChangeLocationPage() { BindingContext = new SettingsViewModel() });

            await PushModalAsync(navPage);
            ShowChangeLocationCommand.ChangeCanExecute();
        }

        public async Task ExecuteContactUsCommand()
        {
            ContactUsCommand.ChangeCanExecute();
            var navPage = new NavigationPage(
                new ContactUsPage() { Title = "Contact Us", BindingContext = new ContactUsPage() });
            await PushModalAsync(navPage);
            ContactUsCommand.ChangeCanExecute();
        }

        public async Task ExecuteSearchCommand(string criteria)
        {
            SearchCommand.ChangeCanExecute();

            // set the data source on each load, because we don't know if the data source may have been updated between page loads
            SetDataSource();

            if (Settings.LocalDataResetIsRequested)
                _subCategories.Clear();

            if (_subCategories.Count < 1 || !Settings.DataIsSeeded || Settings.ClearImageCacheIsRequested)
            {
                var filter = new SubCategoryFilter
                {
                    Criteria = criteria
                };
                await FetchSubCategories(filter, true);
            }

            SearchCommand.ChangeCanExecute();
        }
        #endregion

        #region Private Methods
        private void SetDataSource()
        {
            _dataSource = ServiceLocator.Current.GetInstance<IDataSource<SubCategory, SubCategoryFilter>>();
        }

        private async Task FetchSubCategories(SubCategoryFilter filter, bool forceRefresh = false)
        {
            IsBusy = true;

            filter = filter ?? new SubCategoryFilter();
            filter.ParentId = _selectedCategory?.Id ?? Guid.Empty;

            if (forceRefresh || _allSubCategories == null || _allSubCategories != null && !_allSubCategories.Any())
            {
                _allSubCategories = await _dataSource.GetItems(filter);
                if (_allSubCategories == null || _allSubCategories != null && !_allSubCategories.Any())
                    NoDataFound = true;
                else
                {
                    NoDataFound = false;
                }
                SubCategories = _allSubCategories.ToList();//new List<SubCategory>(result.Where(sc => string.IsNullOrEmpty(filter.Criteria) || sc.Name.Contains(filter.Criteria.ToLower())));
            }

            // ensuring that this flag is reset
            Settings.ClearImageCacheIsRequested = false;

            IsBusy = false;
        }
        #endregion
    }
}
