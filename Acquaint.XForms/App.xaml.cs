﻿using Acquaint.XForms.Pages;
using Acquaint.XForms.ViewModels;
using FormsToolkit;
using OnlineServices.Util;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

//[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace Acquaint.XForms
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();


            // The navigation logic startup needs to diverge per platform in order to meet the UX design requirements
            if (Device.RuntimePlatform == Device.Android)
            {
                // if this is an Android device, set the MainPage to a new SplashPage
                MainPage = new SplashPage();
            }
            else
            {
                MainPage = new NavigationPage(new CategoryListPage()
                {
                    Title = "Categories",
                    BindingContext = new CategoryListViewModel()
                });
            }
        }
    }
}

