﻿using Newtonsoft.Json;
using OnlineServices.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OnlineServices.Data
{
    public class RestService<T>
    {
        HttpClient client;
        private string ControllerName { get; set; }

        public RestService(string controllerName)
        {
            ControllerName = controllerName;
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
        }

        public async Task<string> GetJSON(string actionName, string parameters = "")
        {
            var uri = new Uri(string.Format(Settings.ServerUrl, ControllerName, actionName, parameters));
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            return null;
        }

        public async Task<T> Get(string actionName, string parameters = "")
        {
            var uri = new Uri(string.Format(Settings.ServerUrl, ControllerName, actionName, parameters));
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(content);
            }
            return default(T);
        }

        public async Task<List<T>> GetMany(string actionName, string parameters = "")
        {
            var uri = new Uri(string.Format(Settings.ServerUrl, ControllerName, actionName, parameters));
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<T>>(content);
            }
            return new List<T>();
        }

        public async Task<List<T>> Post(string actionName, object body)
        {
            var uri = new Uri(string.Format(Settings.ServerUrl, ControllerName, actionName, string.Empty));

            var json = JsonConvert.SerializeObject(body);
            var bodyContent = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(uri, bodyContent);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<T>>(content);
            }
            return new List<T>();
        }

        public async Task<string> PostPayload(string actionName, object body)
        {
            var uri = new Uri(string.Format(Settings.ServerUrl, ControllerName, actionName, string.Empty));

            var json = JsonConvert.SerializeObject(body);
            var bodyContent = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(uri, bodyContent);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return content;
            }
            return string.Empty;
        }
    }
}
