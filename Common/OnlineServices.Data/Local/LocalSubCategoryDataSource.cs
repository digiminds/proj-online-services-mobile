﻿using OnlineServices.Abstractions;
using OnlineServices.ModelContracts;
using OnlineServices.Models;
using OnlineServices.Models.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineServices.Data
{
    public class LocalSubCategoryDataSource : IDataSource<SubCategory, SubCategoryFilter>
    {
        bool _IsInitialized;
        List<SubCategory> _SubCategories;

        public Task<SubCategory> GetItem(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<SubCategory>> GetItems()
        {
            await EnsureInitialized().ConfigureAwait(false);
            return await Task.FromResult(_SubCategories.OrderBy(x => x.Name))
            .ConfigureAwait(false);
        }

        public async Task<IEnumerable<SubCategory>> GetItems(SubCategoryFilter filter)
        {
            await EnsureInitialized().ConfigureAwait(false);

            filter = filter ?? (filter = new SubCategoryFilter());
            IEnumerable<SubCategory> subCategories = _SubCategories;

            subCategories = subCategories.Where(sc =>
            {
                return
    (filter.ParentId.Equals(Guid.Empty) || sc.CategoryId.Equals(filter.ParentId)) &&
    (string.IsNullOrEmpty(filter.Criteria) || sc.Title.ToLower().Contains(filter.Criteria.ToLower()));
            });

            return await Task.FromResult(subCategories.OrderBy(x => x.Name))
            .ConfigureAwait(false);
        }

        public Task<bool> AddItem(SubCategory item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> RemoveItem(SubCategory item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateItem(SubCategory item)
        {
            throw new NotImplementedException();
        }

        async Task Initialize()
        {
            await Task.FromResult(_SubCategories = GenerateSubCategories()).ConfigureAwait(false);
            _IsInitialized = true;
        }

        async Task EnsureInitialized()
        {
            if (!_IsInitialized)
                await Initialize().ConfigureAwait(false);
        }

        static List<SubCategory> GenerateSubCategories()
        {
            return new List<SubCategory>
            {
                new SubCategory { Id = new Guid("A1C653C5-2A94-448A-AF13-13B1A3720DD3"), Name = "driver", ArabicTitle = "سائق", Title = "Driver", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "drivers.png"},

                new SubCategory { Id = new Guid("DEB35F12-934C-4FCC-8F41-195659548FA5"), Name = "decoreanddesign", ArabicTitle = "ديكور و تصميم", Title = "Decore & Design", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "decoreanddesign.png"},

                new SubCategory { Id = new Guid("C6B89016-7D82-4158-B073-1B40C144556F"), Name = "hookahdelivery", ArabicTitle = "توصيل نراجيل", Title = "Hookah Delivery", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "hookahdelivery.png"},

                new SubCategory { Id = new Guid("8CE7B401-8BCD-498C-BAF7-2FD7955D90FA"), Name = "carelectrician", ArabicTitle = "كهربائي سيارات", Title = "Car Electrician", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "carelectrician.png"},

                new SubCategory { Id = new Guid("67DAB5C2-0276-4D7D-841C-322CE443317C"), Name = "market", ArabicTitle = "محل بقالة/محل هدايا", Title = "Market/Gift Store", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "market.png"},

                new SubCategory { Id = new Guid("FC94A29E-481B-455A-9D28-34C3EA402841"), Name = "plumber", ArabicTitle = "سمكري", Title = "Plumber", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "plumber.png"},

                new SubCategory { Id = new Guid("9C5EB9D3-E2AA-48D3-A67D-3AD1CF6B002D"), Name = "weddingeventplanning", ArabicTitle = "تنظيم حفلات و أعراس", Title = "Wedding/Event Planning", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "eventplanning.png"},

                new SubCategory { Id = new Guid("84EEEE12-9852-4BF6-83B6-48434702F54A"), Name = "barberformen", ArabicTitle = "حلّاق رجالي", Title = "Barber for Men", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "barbersformen.png"},
                
                new SubCategory { Id = new Guid("63591116-77AA-4DC4-AA52-4F892708F0A1"), Name = "carpenter", ArabicTitle = "نجّار", Title = "Carpenter", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "carpenter.png"},

                new SubCategory { Id = new Guid("78551D5A-945F-487B-933A-7342EDDE2754"), Name = "cleaning", ArabicTitle = "تنظيف", Title = "Cleaning", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "cleaning.png"},

                new SubCategory { Id = new Guid("8616248D-0809-4AF9-AF7A-75FC8C3801B7"), Name = "painter", ArabicTitle = "دهّان", Title = "Painter", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "painter.png"},

                new SubCategory { Id = new Guid("1F347B99-673A-4AA2-9660-81583D25DB85"), Name = "ittechnician", ArabicTitle = "تقنيّ معلوماتية", Title = "It Technician", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "itservices.png"},

                new SubCategory { Id = new Guid("3DDAAF3E-45F1-42FC-AF60-9D280E55D31D"), Name = "cartowing", ArabicTitle = "تحميل سيارات(بلاطة)", Title = "Car Towing", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "cartowing.png"},

                new SubCategory { Id = new Guid("8C0B8669-629C-402D-8D73-9F9CC95B0CC6"), Name = "onlineshops", ArabicTitle = "متاجر الكترونية", Title = "Online Shops", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "onlineshops.png"},

                new SubCategory { Id = new Guid("DDDDC59C-3FD6-44B1-87B4-9FBB0506F634"), Name = "electrician", ArabicTitle = "كهربائي", Title = "Electrician", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "electrician.png"},

                new SubCategory { Id = new Guid("E346F48A-2BDF-4361-BDF3-A67466180060"), Name = "privatetutors", ArabicTitle = "مدرّس خصوصي/معهد", Title = "Private Tutors/Institute", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "tutors.png"},

                new SubCategory { Id = new Guid("0C2F589C-4144-44D6-8381-AAD8D1BF3FE6"), Name = "mechanic", ArabicTitle = "ميكانيكي", Title = "Mechanic", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "mechanic.png"},

                new SubCategory { Id = new Guid("DC4F12A2-1B4C-458E-A35D-AFFBC9E546DD"), Name = "photography", ArabicTitle = "تصوير", Title = "Photography", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "photography.png"},

                new SubCategory { Id = new Guid("67835A05-A900-4F95-B316-B70E0E046A33"), Name = "barberforwomen", ArabicTitle = "حلّاق نسائي", Title = "Barber for Women", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "barbersforwomen.png"},

                new SubCategory { Id = new Guid("D311287E-9EB9-4BB2-8CB1-B946B8BF76D2"), Name = "fridgewashingmashine", ArabicTitle = "مصلّح ثلاجات و غسالات", Title = "Fridge & Washing Machine", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "washingmachinefridge.png"},

                new SubCategory { Id = new Guid("C9529DAC-87B3-4B0A-8969-B9E379411105"), Name = "nursing", ArabicTitle = "عيادة/ممرض(ة)", Title = "Nursing/Clinic", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "nursing.png"},

                new SubCategory { Id = new Guid("2AC6BAF6-8082-4627-B484-C2A77F06E56E"), Name = "airconditioning", ArabicTitle = "تكييف", Title = "Air Conditioning", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "airconditioning.png"},

                new SubCategory { Id = new Guid("7967F9B8-1EC9-4009-9003-C52B4BC985D8"), Name = "laundry", ArabicTitle = "مصبغة/كوي", Title = "Laundry/Ironing", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "laundry.png"},

                new SubCategory { Id = new Guid("B1C29F0B-1193-41AD-96BC-EB9C63B5FFAC"), Name = "watertransport", ArabicTitle = "صهريج مياه", Title = "Water Trasnport", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "watertransport.png"},

                new SubCategory { Id = new Guid("3A83ECAC-2B51-400F-9EB5-F057C9A935C2"), Name = "hardwaretools", ArabicTitle = "خرضوات / تعهدات", Title = "Hardware/ Contracting&Trading", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "hardware.png"},

                new SubCategory { Id = new Guid("E6FC1CD5-F69D-4624-A2A9-F76784EA7127"), Name = "printingservices", ArabicTitle = "خدمات طباعة", Title = "Printing Services", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "printing.png"},

                new SubCategory { Id = new Guid("B3BF0887-A190-4729-A187-BCB1B58DEC9C"), Name = "jewelry", ArabicTitle = "مجوهرات", Title = "Jewelry", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "jewelry.png"},

                new SubCategory { Id = new Guid("F3E61CF5-610E-4BC6-AB0D-EF522E4F2B8D"), Name = "insuranceoffices", ArabicTitle = "مكتب تأمين", Title = "Insurance Office", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "insurance.png"},

                new SubCategory { Id = new Guid("286F92F5-179E-4D79-B7E5-FFC84FA42EC9"), Name = "aluminumandglass", ArabicTitle = "زجاج و ألمنيوم", Title = "Aluminum & Glass", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "aluminumandglass.png"},

                new SubCategory { Id = new Guid("4D7C6246-2942-4846-9FC5-98AB892553E0"), Name = "travelagency", ArabicTitle = "مكتب سفريات", Title = "Travel Agency", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "travel.png"},

                new SubCategory { Id = new Guid("4DE84AA0-2BE7-438C-8608-A71B0AD9E546"), Name = "blacksmith", ArabicTitle = "حدّاد", Title = "Blacksmith", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "blacksmith.png"},

                new SubCategory { Id = new Guid("F9408BB9-D068-4D6A-8CD2-508C69E68170"), Name = "tailor", ArabicTitle = "خيّاط", Title = "Tailor", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "tailor.png"},

                new SubCategory { Id = new Guid("45FA6D93-163E-421E-9DC5-4B32B71FF87E"), Name = "satelliteandcameras", ArabicTitle = "أقمار صناعية و كاميرات", Title = "Satellite & Cameras", CategoryId = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401") , Color = "5A9DB1" ,IconUrl = "satelliteandcameras.png"},

                new SubCategory { Id = new Guid("C0A16B6F-8999-442C-85AB-33161009BBCF"), Name = "shopping", ArabicTitle = "تسوق", Title = "Shopping", CategoryId = new Guid("C0A16B6F-8999-442C-85AB-33161009BBCF") , Color = "5A9DB1" ,IconUrl = "shopping.png"},
            };
        }
    }
}
