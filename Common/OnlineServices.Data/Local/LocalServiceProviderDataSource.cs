﻿using OnlineServices.Abstractions;
using OnlineServices.Models;
using OnlineServices.Models.DTO;
using OnlineServices.Models.Filters;
using OnlineServices.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineServices.Data
{
    public class LocalServiceProviderDataSource : IDataSource<ServiceProviderDTO, ServiceProviderFilter>
    {
        bool _IsInitialized;
        List<ServiceProvider> _ServiceProviders;

        public Task<bool> AddItem(ServiceProviderDTO item)
        {
            throw new NotImplementedException();
        }

        public async Task<ServiceProviderDTO> GetItem(Guid id)
        {
            await EnsureInitialized().ConfigureAwait(false);
            var userCoor = await LocationUtility.GetCurrentCoordinates();
            var serviceProvider = _ServiceProviders.FirstOrDefault(sc => sc.Id.Equals(id));
            return serviceProvider.MapToDto();
        }

        public Task<IEnumerable<ServiceProviderDTO>> GetItems()
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ServiceProviderDTO>> GetItems(ServiceProviderFilter filter)
        {
            await EnsureInitialized().ConfigureAwait(false);

            filter = filter ?? (filter = new ServiceProviderFilter());
            var userCoor = await LocationUtility.GetCurrentCoordinates();
            filter.Latitude = userCoor.Latitude;
            filter.Longitude = userCoor.Longitude;
            IEnumerable<ServiceProvider> serviceProviders = _ServiceProviders;

            serviceProviders = serviceProviders.Where(sc =>
            {
                return
                (filter.ParentId.Equals(Guid.Empty) || sc.SubCategories.Contains(filter.ParentId)) &&
                (!filter.AvailableOnly || sc.Schedule.IsAvailable) &&
                (string.IsNullOrEmpty(filter.Criteria) || sc.FullName.ToLower().Contains(filter.Criteria.ToLower()));
            });

            return serviceProviders.Select(sp => sp.MapToDto()).OrderBy(s => s.Distance);
        }

        public Task<bool> RemoveItem(ServiceProviderDTO item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateItem(ServiceProviderDTO item)
        {
            throw new NotImplementedException();
        }

        async Task Initialize()
        {
            await Task.FromResult(_ServiceProviders = GenerateSubCategories()).ConfigureAwait(false);
            _IsInitialized = true;
        }

        async Task EnsureInitialized()
        {
            if (!_IsInitialized)
                await Initialize().ConfigureAwait(false);
        }

        static List<ServiceProvider> GenerateSubCategories()
        {
            return new List<ServiceProvider>()
            {
                new ServiceProvider() { Id = new Guid("6EB19A75-8CF2-4F70-9201-5BC4E77D4217"), FullName = "Joseph Grimes",          Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("DC29B6BF-8FF1-434C-BD63-CFF94BAF963F"), new Guid("D012B5E7-E3CE-4658-9105-158CC320519E") }, Latitude = 34.154596F, Longitude = 35.659315F, LocationDescription = "Qataah"           ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "0800" , ToHour = "1800"}  },
                new ServiceProvider() { Id = new Guid("4D1BE39A-7031-4F6E-96F7-2C90C37D1B10"), FullName = "Salon Rami",             Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("DC29B6BF-8FF1-434C-BD63-CFF94BAF963F") },                                                   Latitude = 33.811657F, Longitude = 35.697328F, LocationDescription = "Qoubbei"          ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Sunday, FromHour = "1000" , ToHour = "0000"}  },
                new ServiceProvider() { Id = new Guid("B543595B-6D7F-47AB-9470-7C5ED2098FA2"), FullName = "Boutique Muhanad",       Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("DC29B6BF-8FF1-434C-BD63-CFF94BAF963F"), new Guid("D012B5E7-E3CE-4658-9105-158CC320519E") }, Latitude = 33.814225F, Longitude = 35.649477F, LocationDescription = "Baaleshmay"       ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Sunday, FromHour = "1000" , ToHour = "2300"}  },
                new ServiceProvider() { Id = new Guid("EC451B33-D886-41A5-8FFA-36E654CE7544"), FullName = "Salon Mountaha",         Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("D012B5E7-E3CE-4658-9105-158CC320519E") },                                                   Latitude = 33.812274F, Longitude = 35.678363F, LocationDescription = "Qoubbei"          ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "1000" , ToHour = "2300"}  },
                new ServiceProvider() { Id = new Guid("E81ADF11-5048-4001-8E8C-3A0BDEE172AB"), FullName = "Salon Jad",              Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("DC29B6BF-8FF1-434C-BD63-CFF94BAF963F") },                                                   Latitude = 33.854582F, Longitude = 35.725928F, LocationDescription = "Karnayel"         ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "1000" , ToHour = "2300"}  },
                new ServiceProvider() { Id = new Guid("e4029998-5d6e-4ed8-b802-e6f940f307a1"), FullName = "Salon Bou Ali",          Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("DC29B6BF-8FF1-434C-BD63-CFF94BAF963F"), new Guid("D012B5E7-E3CE-4658-9105-158CC320519E") }, Latitude = 33.860928F, Longitude = 35.503486F, LocationDescription = "Goubeiry"         ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "1000" , ToHour = "0000"}  },
                new ServiceProvider() { Id = new Guid("2323e8b6-ed1c-44fe-9cff-90dcd97d3bb5"), FullName = "Salon Mario",            Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("DC29B6BF-8FF1-434C-BD63-CFF94BAF963F"), new Guid("D012B5E7-E3CE-4658-9105-158CC320519E") }, Latitude = 33.955890F, Longitude = 35.605046F, LocationDescription = "Zouk"             ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "1000" , ToHour = "0000"}  },
                new ServiceProvider() { Id = new Guid("00F8A566-2538-4AF7-AE10-997B61537DC0"), FullName = "Garage Garo",            Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("9C0E15D8-CC31-4186-BBE3-B8DA81B43417") },                                                   Latitude = 33.893215F, Longitude = 35.540130F, LocationDescription = "Bourj Hamoud"     ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "0700" , ToHour = "1900"}  },
                new ServiceProvider() { Id = new Guid("FEB64319-1222-4C76-A8E5-EDFF84838B43"), FullName = "Garage El Moustafa",     Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("9C0E15D8-CC31-4186-BBE3-B8DA81B43417") },                                                   Latitude = 33.441793F, Longitude = 35.394031F, LocationDescription = "Zefta"            ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "0700" , ToHour = "1900"}  },
                new ServiceProvider() { Id = new Guid("4FB56717-D2CF-4A5F-894A-C87383A8239D"), FullName = "Garage Elie Saad",       Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("9C0E15D8-CC31-4186-BBE3-B8DA81B43417") },                                                   Latitude = 33.880743F, Longitude = 35.560761F, LocationDescription = "Boushriye"        ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "0700" , ToHour = "1900"}  },
                new ServiceProvider() { Id = new Guid("89149CDA-5B31-4B15-88D2-6011B028F7AE"), FullName = "Garage Abou Shakra",     Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("9C0E15D8-CC31-4186-BBE3-B8DA81B43417") },                                                   Latitude = 33.853555F, Longitude = 35.527309F, LocationDescription = "Hadath"           ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "0700" , ToHour = "1900"}  },
                new ServiceProvider() { Id = new Guid("BFD74C2A-7840-45DD-9C47-13F90DE01F8B"), FullName = "Elie Azzi For Honda",    Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("9C0E15D8-CC31-4186-BBE3-B8DA81B43417") },                                                   Latitude = 33.893388F, Longitude = 35.547845F, LocationDescription = "Dora"             ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "0700" , ToHour = "1900"}  },
                new ServiceProvider() { Id = new Guid("CF568F71-C4B6-45A9-A922-C9E69EF17B49"), FullName = "Amer Abdel Rahim",       Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("9C0E15D8-CC31-4186-BBE3-B8DA81B43417") },                                                   Latitude = 33.773177F, Longitude = 35.900534F, LocationDescription = "Bar Elias"        ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "0700" , ToHour = "1900"}  },
                new ServiceProvider() { Id = new Guid("D81687FD-FE84-4F0F-AFDD-A104B28EC1A7"), FullName = "Al Sho3la",              Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("D07F5E7D-7C62-49FC-9253-8A1047EA7956") },                                                   Latitude = 33.692560F, Longitude = 35.507695F, LocationDescription = "Dmit"             ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "0700" , ToHour = "2100"}  },
                new ServiceProvider() { Id = new Guid("FB580ADA-4381-43EE-ADA1-D072878F08CE"), FullName = "Orbit",                  Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("D07F5E7D-7C62-49FC-9253-8A1047EA7956") },                                                   Latitude = 33.918086F, Longitude = 35.693416F, LocationDescription = "Bekfaya"          ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "0700" , ToHour = "2100"}  },
                new ServiceProvider() { Id = new Guid("953d9588-e6be-49cf-881d-68431b8285c3"), FullName = "Salah Zeineddine",       Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("D07F5E7D-7C62-49FC-9253-8A1047EA7956") },                                                   Latitude = 33.812274F, Longitude = 35.678363F, LocationDescription = "Qoubbei"          ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "0700" , ToHour = "2100"}  },
                new ServiceProvider() { Id = new Guid("450fe593-433f-4bca-9f39-f2a0e4c64dc6"), FullName = "Salim El Sangari",       Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("D07F5E7D-7C62-49FC-9253-8A1047EA7956") },                                                   Latitude = 33.847143F, Longitude = 35.681291F, LocationDescription = "Ras El Matn"      ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "0700" , ToHour = "2100"}  },
                new ServiceProvider() { Id = new Guid("5c957b8f-6e76-470c-941f-789d12f10a42"), FullName = "Itani Plumbering",       Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("D07F5E7D-7C62-49FC-9253-8A1047EA7956") },                                                   Latitude = 33.893667F, Longitude = 35.485386F, LocationDescription = "Snoubra"          ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "0700" , ToHour = "2100"}  },
                new ServiceProvider() { Id = new Guid("6FEFF721-2A97-4C0F-AACB-30B1F521ABF6"), FullName = "Abo Sifona",             Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("D07F5E7D-7C62-49FC-9253-8A1047EA7956") },                                                   Latitude = 33.788394F, Longitude = 35.483138F, LocationDescription = "Khalde"           ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "0700" , ToHour = "2100"}  },
                new ServiceProvider() { Id = new Guid("CA0A6161-6898-421D-9F29-A51B60F36BEE"), FullName = "Ceramica l Beshbeshi",   Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("D07F5E7D-7C62-49FC-9253-8A1047EA7956") },                                                   Latitude = 33.779843F, Longitude = 35.530812F, LocationDescription = "Bshamoun"         ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "0700" , ToHour = "2100"}  },
                new ServiceProvider() { Id = new Guid("9CD6310F-1439-4898-9F51-EEC96D032CD3"), FullName = "Gaelle Abou Sleimen",    Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("65564CCC-760F-4EEB-8E37-136E7D88B082") },                                                   Latitude = 33.975037F, Longitude = 35.623804F, LocationDescription = "Sarba"            ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "1400" , ToHour = "2100"}  },
                new ServiceProvider() { Id = new Guid("D5E85894-129F-4F39-A75D-893DAB128ECD"), FullName = "Ma3had l najah",         Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("65564CCC-760F-4EEB-8E37-136E7D88B082") },                                                   Latitude = 33.873069F, Longitude = 35.697071F, LocationDescription = "Salima"           ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "1400" , ToHour = "2100"}  },
                new ServiceProvider() { Id = new Guid("6CF4DE3E-FE50-4860-8E5C-6DCF479D4737"), FullName = "Khaled Hatoum",          Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("65564CCC-760F-4EEB-8E37-136E7D88B082") },                                                   Latitude = 33.853869F, Longitude = 35.778600F, LocationDescription = "Kfarselwan"       ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "1400" , ToHour = "2100"}  },
                new ServiceProvider() { Id = new Guid("DAFB9C5C-54A3-4F18-BC01-10AD2491AEC7"), FullName = "Ali Starbucks",          Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("65564CCC-760F-4EEB-8E37-136E7D88B082") },                                                   Latitude = 33.891843F, Longitude = 35.477675F, LocationDescription = "Madam Curie"      ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "1400" , ToHour = "2100"}  },
                new ServiceProvider() { Id = new Guid("AB6F1601-94F3-4E32-A08A-089B5B52DA36"), FullName = "Dr. Najah Maarouf",      Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("65564CCC-760F-4EEB-8E37-136E7D88B082") },                                                   Latitude = 33.866754F, Longitude = 35.498643F, LocationDescription = "Tariq El Jdideh"  ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "1400" , ToHour = "2100"}  },
                new ServiceProvider() { Id = new Guid("70EB3223-4ED2-4FE2-9AC1-F72B474FF05F"), FullName = "Dr. Wahid Bou Maslaha",  Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("65564CCC-760F-4EEB-8E37-136E7D88B082") },                                                   Latitude = 33.897485F, Longitude = 35.490140F, LocationDescription = "Beirut"           ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "1400" , ToHour = "2100"}  },
                new ServiceProvider() { Id = new Guid("A5A8F111-FE08-4478-A90B-222F4BA033DD"), FullName = "Mr. Fidaa Bou Sanaa",    Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("65564CCC-760F-4EEB-8E37-136E7D88B082") },                                                   Latitude = 33.807670F, Longitude = 35.597681F, LocationDescription = "Aley"             ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "1400" , ToHour = "2100"}  },
                new ServiceProvider() { Id = new Guid("6348C5F4-2073-4868-959C-D1650FD8C186"), FullName = "Nasser Manajah",         Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("65564CCC-760F-4EEB-8E37-136E7D88B082") },                                                   Latitude = 33.976410F, Longitude = 35.613657F, LocationDescription = "Kaslik"           ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "1400" , ToHour = "2100"}  },
                new ServiceProvider() { Id = new Guid("303A5E88-E91D-43ED-9391-FDE9F7C03A66"), FullName = "Fadi Ma sa2at",          Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("65564CCC-760F-4EEB-8E37-136E7D88B082") },                                                   Latitude = 33.977720F, Longitude = 35.800863F, LocationDescription = "Bqaatouta"        ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "1400" , ToHour = "2100"}  },
                new ServiceProvider() { Id = new Guid("0782C981-F003-44A4-87D1-771D3C6EB6B3"), FullName = "Texas Tutoring Center",  Description = "Visit us to learn more", PhoneNumber = "+9613345504", MobileNumber = "+96171918618", Email = "l.awar@bsynchro.com", Rate = 15000, Currency = "LBP", SubCategories = new List<Guid>() { new Guid("65564CCC-760F-4EEB-8E37-136E7D88B082") },                                                   Latitude = 33.811441F, Longitude = 35.697549F, LocationDescription = "Qoubbei"          ,Schedule = new Schedule() {FromDay = DayOfWeek.Monday, ToDay = DayOfWeek.Friday, FromHour = "1400" , ToHour = "2100"}  }
            };
        }
    }
}
