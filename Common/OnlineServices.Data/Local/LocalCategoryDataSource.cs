﻿using OnlineServices.Abstractions;
using OnlineServices.ModelContracts;
using OnlineServices.Models;
using OnlineServices.Models.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineServices.Data
{
    public class LocalCategoryDataSource : IDataSource<Category, CategoryFilter>
    {
        bool _IsInitialized;
        List<Category> _Categories;

        public Task<Category> GetItem(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Category>> GetItems()
        {
            await EnsureInitialized().ConfigureAwait(false);
            return await Task.FromResult(_Categories).ConfigureAwait(false);
        }

        public async Task<IEnumerable<Category>> GetItems(CategoryFilter filter)
        {
            await EnsureInitialized().ConfigureAwait(false);
            return await Task.FromResult(_Categories.Where(sc => sc.Title.ToLower().Contains(filter.Criteria.ToLower())))
                .ConfigureAwait(false);
        }

        public Task<bool> AddItem(Category item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> RemoveItem(Category item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateItem(Category item)
        {
            throw new NotImplementedException();
        }

        async Task Initialize()
        {
            await Task.FromResult(_Categories = GenerateCategories()).ConfigureAwait(false);           
            _IsInitialized = true;
        }

        async Task EnsureInitialized()
        {
            if (!_IsInitialized)
                await Initialize().ConfigureAwait(false);
        }

        static List<Category> GenerateCategories()
        {
            return new List<Category>()
            {
                new Category { Id = new Guid("00004363-F79A-44E7-BC32-6128E2EC8401"), Name = "services",  Title = "Services",     ArabicTitle = "خدمات",  IconUrl = "services.png"},
                new Category { Id = new Guid("C0A16B6F-8999-442C-85AB-33161009BBCF"), Name = "shopping",  Title = "Shopping",     ArabicTitle = "تسوق",  IconUrl = "shopping.png"},
                new Category { Id = new Guid("c227bfd2-c6f6-49b5-93ec-afef9eb18d08"), Name = "foodsanddrinks",      Title = "Foods & Drinks",         ArabicTitle = "أكل و شرب",      IconUrl = "restaurantbakery.png" },
                new Category { Id = new Guid("31bf6fe5-18f1-4354-9571-2cdecb0c00af"), Name = "job",     Title = "Jobs",        ArabicTitle = "وظائف",     IconUrl = "jobs.png" },
                new Category { Id = new Guid("45d2ddc0-a8e9-4aea-8b51-2860c708e30d"), Name = "apartment", Title = "Real Estate",    ArabicTitle = "شقق و عقارات", IconUrl = "apartment.png" }                
            };
        }
    }
}
