﻿using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using Microsoft.WindowsAzure.MobileServices.Sync;
using OnlineServices.Abstractions;
using OnlineServices.Models;
using OnlineServices.Models.Filters;
using OnlineServices.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineServices.Data.Remote
{
    public class SubCategoryDataSource : BaseDataSource, IDataSource<SubCategory, SubCategoryFilter>
    {
        #region Fields
        private readonly Lazy<RestService<SubCategory>> _restService;
        #endregion

        #region Ctor
        public SubCategoryDataSource()
        {
            _restService = new Lazy<RestService<SubCategory>>(() => new RestService<SubCategory>("SubCategory"));
        }
        #endregion

        #region Properties
        public RestService<SubCategory> RestService => _restService.Value;
        #endregion

        public Task<SubCategory> GetItem(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<SubCategory>> GetItems()
        {
            return await Execute<IEnumerable<SubCategory>>(async () => await RestService.GetMany(nameof(GetItems)),
                new List<SubCategory>()).ConfigureAwait(false);
        }

        public async Task<IEnumerable<SubCategory>> GetItems(SubCategoryFilter filter)
        {
            filter = filter ?? new SubCategoryFilter();

            return await Execute<IEnumerable<SubCategory>>(async () => await RestService.Post(nameof(GetItems), filter),
                new List<SubCategory>()).ConfigureAwait(false);
        }

        public Task<bool> AddItem(SubCategory item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> RemoveItem(SubCategory item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateItem(SubCategory item)
        {
            throw new NotImplementedException();
        }
    }
}
