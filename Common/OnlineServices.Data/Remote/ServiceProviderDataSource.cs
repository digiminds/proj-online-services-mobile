﻿using OnlineServices.Abstractions;
using OnlineServices.Models;
using OnlineServices.Models.DTO;
using OnlineServices.Models.Filters;
using OnlineServices.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineServices.Data.Remote
{
    public class ServiceProviderDataSource : BaseDataSource, IDataSource<ServiceProviderDTO, ServiceProviderFilter>
    {
        #region Fields
        private readonly Lazy<RestService<ServiceProvider>> _restService;
        #endregion

        #region Ctor
        public ServiceProviderDataSource()
        {
            _restService = new Lazy<RestService<ServiceProvider>>(() => new RestService<ServiceProvider>("ServiceProvider"));
        }
        #endregion

        #region Properties
        public RestService<ServiceProvider> RestService => _restService.Value;
        #endregion

        public async Task<ServiceProviderDTO> GetItem(Guid id)
        {
            return await Execute<ServiceProviderDTO>(async () =>
            {
                var result = await RestService.Get(nameof(GetItem), "?id=" + id);
                return result.MapToDto();
            }, new ServiceProviderDTO()).ConfigureAwait(false);
        }

        public Task<IEnumerable<ServiceProviderDTO>> GetItems()
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ServiceProviderDTO>> GetItems(ServiceProviderFilter filter)
        {
            filter = filter ?? new ServiceProviderFilter();
            var userCoor = await LocationUtility.GetCurrentCoordinates();
            filter.Latitude = userCoor?.Latitude ?? 0;
            filter.Longitude = userCoor?.Longitude ?? 0;
            filter.CurrentTime = DateTime.Now.ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss");
            return await Execute(async () =>
            {
                var results = await RestService.Post(nameof(GetItems), filter);
                return results.Select(res => res.MapToDto());
            }, new List<ServiceProviderDTO>()).ConfigureAwait(false);
        }

        public Task<bool> AddItem(ServiceProviderDTO item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> RemoveItem(ServiceProviderDTO item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateItem(ServiceProviderDTO item)
        {
            throw new NotImplementedException();
        }
    }
}
