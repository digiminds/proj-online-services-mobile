﻿using Microsoft.Practices.ServiceLocation;
using OnlineServices.Abstractions;
using OnlineServices.ModelContracts;
using OnlineServices.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OnlineServices.Data.Remote
{
    public abstract class BaseDataSource
    {
        #region Exception Helpers            
        protected static async Task Execute(Func<Task> execute)
        {
            try
            {
                await execute().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                HandleExceptions(ex);
            }
        }

        protected static async Task<T> Execute<T>(Func<Task<T>> execute, T defaultReturnObject)
        {
            try
            {
                return await execute().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                HandleExceptions(ex);
            }
            return defaultReturnObject;
        }

        private static void HandleExceptions(Exception ex)
        {
            if (ex != null)
            {
                // TODO: report with HockeyApp
                System.Diagnostics.Debug.WriteLine($"ERROR {ex.Message}");
            }
        }
        #endregion
    }
}
