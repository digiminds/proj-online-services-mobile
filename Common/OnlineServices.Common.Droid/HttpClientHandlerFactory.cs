﻿using System.Net.Http;
using OnlineServices.Abstractions;

namespace OnlineServices.Common.Droid
{
	public class HttpClientHandlerFactory : IHttpClientHandlerFactory
	{
		public HttpClientHandler GetHttpClientHandler()
		{
			// not needed on Android
			return null;
		}
	}
}

