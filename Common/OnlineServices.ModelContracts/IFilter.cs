﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineServices.ModelContracts
{
    public interface IFilter
    {
        Guid ParentId { get; set; }
        string Criteria { get; set; }
    }
}
