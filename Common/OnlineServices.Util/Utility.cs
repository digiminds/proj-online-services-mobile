﻿using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineServices.Util
{
    #region Location Utility
    public static class TimeUtility
    {
        public static string GetHourString(string time)
        {
            if (string.IsNullOrEmpty(time) || time.Length != 4) return "12:00 am";

            try
            {
                var hours = int.Parse(time.Substring(0, 2));
                var minutes = int.Parse(time.Substring(2, 2));

                var datetime = new DateTime(2000, 1, 1, hours, minutes, 0);

                return datetime.ToString("hh:mm tt");
            }
            catch (Exception)
            {
                return "12:00 am";
            }
        }

        public static bool GetIsAvailable(DayOfWeek fromDay, DayOfWeek toDay, string fromHourString, string toHourString)
        {
            try
            {
                var now = DateTime.Now;
                var time = int.Parse(now.ToString("HHmm"));
                bool opensToday = false;
                if (fromDay == toDay)
                    opensToday = true;
                else if (fromDay < toDay)
                    opensToday = now.DayOfWeek >= fromDay && now.DayOfWeek <= toDay;
                else if (fromDay > toDay)
                    opensToday = now.DayOfWeek <= fromDay && now.DayOfWeek >= toDay;

                bool opensNow = false;
                if (opensToday)
                {
                    int fromHour = int.Parse(fromHourString);
                    int toHour = int.Parse(toHourString);
                    if (fromHour == toHour)
                        opensNow = true;
                    else if (toHour == 0 || fromHour < toHour)
                        opensNow = time >= fromHour && (time <= toHour || toHour == 0);
                    else if (fromHour == 0 || fromHour > toHour)
                        opensNow = (time <= fromHour || fromHour == 0) && time >= toHour;
                }

                return opensToday && opensNow;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
    #endregion

    #region Location Utility
    public static class LocationUtility
    {
        public static async Task<Coordinates> GetCurrentCoordinates()
        {
            if (Settings.IsUseCurrentLocation && string.IsNullOrEmpty(Settings.UserSelectedLocationCoordinates))
            {
                var position = await GetCurrentPosition(5000);
                if (position != null)
                {
                    Settings.UserSelectedLocationCoordinates = $"{position.Latitude};{position.Longitude}";
                    return new Coordinates(position.Latitude, position.Longitude);
                }
            }
            else
            {
                var split = Settings.UserSelectedLocationCoordinates.Split(';');

                if (split.Length == 2)
                {
                    double lat, lon;
                    if (double.TryParse(split[0], out lat) && double.TryParse(split[1], out lon))
                        return new Coordinates(lat, lon);
                }
            }

            return null;
        }

        public static async Task<Position> GetCurrentPosition(int timespan = 10000)
        {
            try
            {
                Position position = null;
                if (CrossGeolocator.IsSupported)
                {
                    var locator = CrossGeolocator.Current;
                    if (locator == null) throw new Exception();

                    locator.DesiredAccuracy = 50;
                    TimeSpan ts = TimeSpan.FromMilliseconds(timespan);
                    if (locator.IsGeolocationEnabled)
                    {
                        position = await locator.GetPositionAsync(ts);
                    }

                    if (position == null)
                    {
                        position = await locator.GetLastKnownLocationAsync();
                    }
                }

                return position;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static Position GetCurrentPositionSync(int timespan = 10000)
        {
            try
            {
                if (!CrossGeolocator.IsSupported) return null;

                Position position = null;
                var locator = CrossGeolocator.Current;
                if (locator == null) throw new Exception();

                locator.DesiredAccuracy = 50;
                var ts = TimeSpan.FromMilliseconds(timespan);
                if (locator.IsGeolocationEnabled)
                {
                    position = locator.GetPositionAsync(ts).Result;
                }

                return position ?? locator.GetLastKnownLocationAsync().Result;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
    #endregion

    #region Distance Utility
    public static class DistanceUtility
    {
        public static double GetDistance(Coordinates providerCoor, Coordinates userCoordinates, UnitOfLength unitOfLength)
        {
            double distance = 0;
            if (userCoordinates != null)
            {
                distance = new Coordinates(providerCoor.Latitude, providerCoor.Longitude)
                        .DistanceTo(new Coordinates(userCoordinates.Latitude, userCoordinates.Longitude), unitOfLength);
            }
            return distance;
        }
    }
    public class Coordinates
    {
        public double Latitude { get; private set; }
        public double Longitude { get; private set; }

        public Coordinates(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }
    }
    public static class CoordinatesDistanceExtensions
    {
        public static double DistanceTo(this Coordinates baseCoordinates, Coordinates targetCoordinates)
        {
            return DistanceTo(baseCoordinates, targetCoordinates, UnitOfLength.Kilometers);
        }

        public static double DistanceTo(this Coordinates baseCoordinates, Coordinates targetCoordinates, UnitOfLength unitOfLength)
        {
            var baseRad = Math.PI * baseCoordinates.Latitude / 180;
            var targetRad = Math.PI * targetCoordinates.Latitude / 180;
            var theta = baseCoordinates.Longitude - targetCoordinates.Longitude;
            var thetaRad = Math.PI * theta / 180;

            double dist =
                Math.Sin(baseRad) * Math.Sin(targetRad) + Math.Cos(baseRad) *
                Math.Cos(targetRad) * Math.Cos(thetaRad);
            dist = Math.Acos(dist);

            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;

            return unitOfLength.ConvertFromMiles(dist);
        }
    }
    public class UnitOfLength
    {
        public static UnitOfLength Kilometers = new UnitOfLength(1.609344);
        public static UnitOfLength NauticalMiles = new UnitOfLength(0.8684);
        public static UnitOfLength Miles = new UnitOfLength(1);

        private readonly double _fromMilesFactor;

        private UnitOfLength(double fromMilesFactor)
        {
            _fromMilesFactor = fromMilesFactor;
        }

        public double ConvertFromMiles(double input)
        {
            return input * _fromMilesFactor;
        }
    }
    #endregion
}
