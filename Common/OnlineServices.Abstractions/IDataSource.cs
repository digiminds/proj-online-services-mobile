﻿using OnlineServices.ModelContracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnlineServices.Abstractions
{
    /// <summary>
    /// Defines a conract for data source that exposes CRUD operations for a specific type.
    /// </summary>
    public interface IDataSource<T, TFilter> 
        where T : IObservableEntityData
        where TFilter : IFilter
    {
        /// <summary>
        /// Gets all the items.
        /// </summary>
        /// <returns>All the items.</returns>
        Task<IEnumerable<T>> GetItems();

        /// <summary>
        /// Get filtered items
        /// </summary>
        /// <param name="filter"></param>
        /// <returns>Filtered Items</returns>
        Task<IEnumerable<T>> GetItems(TFilter filter);

        /// <summary>
        /// Gets an item.
        /// </summary>
        /// <returns>An item.</returns>
        /// <param name="id">Id of item to retrieve.</param>
        Task<T> GetItem(Guid id);

        /// <summary>
        /// Adds an item.
        /// </summary>
        /// <returns>A bool representing whether or not the operation succeeded.</returns>
        /// <param name="item">An item.</param>
        Task<bool> AddItem(T item);

        /// <summary>
        /// Updates an item.
        /// </summary>
        /// <returns>A bool representing whether or not the operation succeeded.</returns>
        /// <param name="item">An item.</param>
        Task<bool> UpdateItem(T item);

        /// <summary>
        /// Removes an item.
        /// </summary>
        /// <returns>A bool representing whether or not the operation succeeded.</returns>
        /// <param name="item">An item.</param>
        Task<bool> RemoveItem(T item);
    }
}
