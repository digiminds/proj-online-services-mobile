﻿using System;
using OnlineServices.Abstractions;
using Microsoft.WindowsAzure.MobileServices;
using MvvmHelpers;
using Newtonsoft.Json;

namespace OnlineServices.Models
{
	/// <summary>
	/// A type that mirrors the properties of Microsoft.Azure.Mobile.Server.EntityData, and is also observable.
	/// </summary>
	public class ObservableEntityData : ObservableObject, IObservableEntityData
	{
		public ObservableEntityData()
		{
		}

		[JsonProperty(PropertyName = "id")]
		public Guid Id { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "Title")]
        public string Title { get; set; }

        [CreatedAt]
		public DateTimeOffset CreatedAt { get; set; }

		[UpdatedAt]
		public DateTimeOffset UpdatedAt { get; set; }

		[Version]
		public byte[] Version { get; set; }

        public string DataPartitionId { get; set; }
    }
}

