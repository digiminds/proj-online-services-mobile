﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineServices.Models.DTO
{
    public class ServiceProviderDTO : ServiceProvider
    {
        public string DistanceString { get; set; }
        public string IsAvailableString { get; set; }
        public string AvailabilityString { get; set; }
        public string AvailabilityIcon { get; set; }
        public string RateString { get; set; }
        public string FromHourString { get; set; }
        public string ToHourString { get; set; }
        public string ProfilePhotoUrl { get; set; }
    }
}
