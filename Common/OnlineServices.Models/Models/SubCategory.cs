﻿using OnlineServices.ModelContracts;
using System;

namespace OnlineServices.Models
{
    public class SubCategory : ObservableEntityData
    {
        #region Properties
        public string ArabicTitle { get; set; }
        public Guid CategoryId { get; set; }
        public string IconUrl { get; set; }
        public string Color { get; set; }
        #endregion

        #region Ctor
        public SubCategory()
        {

        }
        #endregion
    }
}
