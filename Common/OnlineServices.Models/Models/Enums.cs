﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineServices.Models
{
    public class Enums
    {
        public enum SearchType
        {
            All,
            Category,
            SubCategory,
            ServiceProvider
        }
    }
}
