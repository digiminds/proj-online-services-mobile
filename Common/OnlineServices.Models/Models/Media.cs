﻿using OnlineServices.ModelContracts;
using System;

namespace OnlineServices.Models
{
    public class Media
    {
        #region Properties
        public string FileName { get; set; }
        public byte[] Content { get; set; }
        #endregion

        #region Ctor
        public Media()
        {

        }
        #endregion
    }
}
