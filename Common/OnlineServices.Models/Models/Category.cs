﻿using OnlineServices.ModelContracts;
using System;

namespace OnlineServices.Models
{
    public class Category : ObservableEntityData
    {
        #region Properties
        public string ArabicTitle { get; set; }
        public string IconUrl { get; set; }
        public string Description { get; set; }
        public Media Media { get; set; }
        #endregion

        #region Ctor
        public Category()
        {
            Media = new Media();
        }
        #endregion
    }
}
