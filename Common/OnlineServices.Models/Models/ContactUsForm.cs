﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineServices.Models.Models
{
    public class ContactUsForm : ObservableEntityData
    {
        #region Properties

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Company { get; set; }
        public string Title { get; set; }

        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public string Address { get; set; }

        public string MessageContent { get; set; }



        #endregion

        #region ctor

        public ContactUsForm() { }

        #endregion
    }
}
