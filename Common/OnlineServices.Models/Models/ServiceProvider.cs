﻿using System;
using System.Collections.Generic;
using OnlineServices.ModelContracts;
using OnlineServices.Util;
using OnlineServices.Models.DTO;
using System.Threading.Tasks;

namespace OnlineServices.Models
{
    public class ServiceProvider : ObservableEntityData
    {
        #region Properties
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public decimal Rate { get; set; }
        public string Currency { get; set; }
        public IEnumerable<Guid> SubCategories { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string LocationDescription { get; set; }
        public Schedule Schedule { get; set; }
        public double Distance { get; set; }
        #endregion

        #region Ctor
        public ServiceProvider()
        {
            SubCategories = new List<Guid>();
        }
        #endregion

        #region Public Method
        public ServiceProviderDTO MapToDto()
        {
            var dto = new ServiceProviderDTO
            {
                Id = Id,
                FullName = FullName,
                Email = Email,
                MobileNumber = MobileNumber,
                PhoneNumber = PhoneNumber,
                Longitude = Longitude,
                Latitude = Latitude,
                LocationDescription = LocationDescription,
                Description = Description,
                Facebook = Facebook,
                Instagram = Instagram,
                Rate = Rate,
                RateString = $"{Rate} {Currency}",
                DistanceString = Distance > 0 ? $"~ {Distance:#.##} Km" : string.Empty
            };

            if (Schedule?.FromHour != null && Schedule?.ToHour != null)
            {
                dto.FromHourString = TimeUtility.GetHourString(Schedule.FromHour);
                dto.ToHourString = TimeUtility.GetHourString(Schedule.ToHour);
                dto.IsAvailableString = Schedule.IsAvailable ? "Open" : "Closed";
                dto.AvailabilityString =
                    $"{Schedule.FromDay.ToString()} to {Schedule.ToDay.ToString()} - {dto.FromHourString} till {dto.ToHourString}";
                dto.AvailabilityIcon = Schedule.IsAvailable ? "icon_available.png" : "icon_unavailable.png";
            }
            dto.ProfilePhotoUrl = string.Format(Settings.ServerUrl, "serviceprovider", "getproviderimage", "?providerId=" + Id);
            return dto;
        }
        #endregion
    }
}
