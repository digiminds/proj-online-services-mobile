﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineServices.Models
{
    public class Schedule
    {
        public DayOfWeek FromDay { get; set; }
        public DayOfWeek ToDay { get; set; }
        public string FromHour { get; set; }
        public string ToHour { get; set; }
        public bool IsAvailable { get; set; }
    }
}
