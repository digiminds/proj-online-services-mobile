﻿using OnlineServices.ModelContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineServices.Models.Filters
{
    public class CategoryFilter : IFilter
    {
        public Guid ParentId { get; set; }
        public string Criteria { get; set; }
    }
}
