﻿using OnlineServices.ModelContracts;
using OnlineServices.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineServices.Models.Filters
{
    public class ServiceProviderFilter : IFilter
    {
        public Guid ParentId { get; set; }
        public string Criteria { get; set; }
        public bool AvailableOnly { get; set; }
        public string CurrentTime { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
