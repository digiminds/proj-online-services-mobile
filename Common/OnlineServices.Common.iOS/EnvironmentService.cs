﻿using OnlineServices.Abstractions;
using ObjCRuntime;

namespace OnlineServices.Common.iOS
{
    public class EnvironmentService : IEnvironmentService
    {
        #region IEnvironmentService implementation

        public bool IsRealDevice => Runtime.Arch == Arch.DEVICE;

        #endregion
    }
}

