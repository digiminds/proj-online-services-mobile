﻿using System;
using Autofac;
using Autofac.Extras.CommonServiceLocator;
using FFImageLoading.Forms.Touch;
using Foundation;
using HockeyApp.iOS;
using Microsoft.Practices.ServiceLocation;
using OnlineServices.Abstractions;
using OnlineServices.Common.iOS;
using OnlineServices.Data;
using OnlineServices.Data.Remote;
using OnlineServices.Models;
using OnlineServices.Models.DTO;
using OnlineServices.Models.Filters;
using OnlineServices.Util;
using UIKit;
using Xamarin;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

namespace Acquaint.XForms.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : FormsApplicationDelegate
    {
        // an IoC Container
        IContainer _IoCContainer;

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            //var manager = BITHockeyManager.SharedHockeyManager;
            //manager.Configure(Settings.HockeyAppId);
            //manager.StartManager();

            RegisterDependencies();

            //Settings.OnDataPartitionPhraseChanged += (sender, e) =>
            //{
            //    UpdateDataSourceIfNecessary();
            //};

#if ENABLE_TEST_CLOUD
            //Xamarin.Calabash.Start();
#endif

            Forms.Init();

            Microsoft.WindowsAzure.MobileServices.CurrentPlatform.Init();

            FormsMaps.Init();

            CachedImageRenderer.Init();

            LoadApplication(new App());

            ConfigureTheming();

            return base.FinishedLaunching(app, options);
        }

        void ConfigureTheming()
        {
            UINavigationBar.Appearance.TintColor = UIColor.White;
            UINavigationBar.Appearance.BarTintColor = Color.FromHex("c62828").ToUIColor();
            UINavigationBar.Appearance.TitleTextAttributes = new UIStringAttributes { ForegroundColor = UIColor.White };
            UIBarButtonItem.Appearance.SetTitleTextAttributes(new UITextAttributes { TextColor = UIColor.White }, UIControlState.Normal);
        }

        /// <summary>
        /// Registers dependencies with an IoC container.
        /// </summary>
        /// <remarks>
        /// Since some of our libraries are shared between the Forms and Native versions 
        /// of this app, we're using an IoC/DI framework to provide access across implementations.
        /// </remarks>
        void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance(new EnvironmentService()).As<IEnvironmentService>();

            builder.RegisterInstance(new HttpClientHandlerFactory()).As<IHttpClientHandlerFactory>();


            // Set the data source dependent on whether or not the data parition phrase is "UseLocalDataSource".
            // The local data source is mainly for use in TestCloud test runs, but the app can be used in local-only data mode if desired.
            if (Settings.IsUsingLocalDataSource)
            {
                builder.RegisterInstance(LazyLocalCategoryDataSource.Value)
                    .As<IDataSource<Category, CategoryFilter>>();
                builder.RegisterInstance(LazyLocalSubCategoryDataSource.Value)
                    .As<IDataSource<SubCategory, SubCategoryFilter>>();
                builder.RegisterInstance(LazyLocalServiceProviderDataSource.Value)
                    .As<IDataSource<ServiceProviderDTO, ServiceProviderFilter>>();
            }
            else
            {
                builder.RegisterInstance(LazyCategoryDataSource.Value)
                    .As<IDataSource<Category, CategoryFilter>>();
                builder.RegisterInstance(LazySubCategoryDataSource.Value)
                    .As<IDataSource<SubCategory, SubCategoryFilter>>();
                builder.RegisterInstance(LazyServiceProviderDataSource.Value)
                    .As<IDataSource<ServiceProviderDTO, ServiceProviderFilter>>();
            }

            _IoCContainer = builder.Build();

            var csl = new AutofacServiceLocator(_IoCContainer);
            ServiceLocator.SetLocatorProvider(() => csl);
        }

        /// <summary>
		/// Updates the data source if necessary.
		/// </summary>
		void UpdateDataSourceIfNecessary()
        {
            var dataSource = ServiceLocator.Current.GetInstance<IDataSource<Category, CategoryFilter>>();

            // Set the data source dependent on whether or not the data parition phrase is "UseLocalDataSource".
            // The local data source is mainly for use in TestCloud test runs, but the app can be used in local-only data mode if desired.

            // if the settings dictate that a local data source should be used, then register the local data provider and update the IoC container
            if (Settings.IsUsingLocalDataSource && !(dataSource is LocalCategoryDataSource))
            {
                var builder = new ContainerBuilder();
                builder.RegisterInstance(LazyLocalSubCategoryDataSource.Value).As<IDataSource<Category, CategoryFilter>>();
                builder.Update(_IoCContainer);
                return;
            }

            // if the settings dictate that a local data souce should not be used, then register the remote data source and update the IoC container
            if (!Settings.IsUsingLocalDataSource && !(dataSource is SubCategoryDataSource))
            {
                var builder = new ContainerBuilder();
                builder.RegisterInstance(LazySubCategoryDataSource.Value).As<IDataSource<Category, CategoryFilter>>();
                builder.Update(_IoCContainer);
            }
        }

        // we need lazy loaded instances of these two types hanging around because if the registration on IoC container changes at runtime, we want the same instances
        //REMOTE //Sub Category is reading data locally
        private static readonly Lazy<LocalCategoryDataSource> LazyCategoryDataSource = new Lazy<LocalCategoryDataSource>(() => new LocalCategoryDataSource());
        private static readonly Lazy<SubCategoryDataSource> LazySubCategoryDataSource = new Lazy<SubCategoryDataSource>(() => new SubCategoryDataSource());
        private static readonly Lazy<ServiceProviderDataSource> LazyServiceProviderDataSource = new Lazy<ServiceProviderDataSource>(() => new ServiceProviderDataSource());
        //LOCAL
        private static readonly Lazy<LocalCategoryDataSource> LazyLocalCategoryDataSource = new Lazy<LocalCategoryDataSource>(() => new LocalCategoryDataSource());
        private static readonly Lazy<LocalSubCategoryDataSource> LazyLocalSubCategoryDataSource = new Lazy<LocalSubCategoryDataSource>(() => new LocalSubCategoryDataSource());
        private static readonly Lazy<LocalServiceProviderDataSource> LazyLocalServiceProviderDataSource = new Lazy<LocalServiceProviderDataSource>(() => new LocalServiceProviderDataSource());
    }
}

